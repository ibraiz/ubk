package com.ubroadkast.nayatel;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeScreen extends ListActivity {

    String login_username;
    String login_password;
    Intent i;
    String url = "http://ubroadkast.com/sample.php";
    public static String jsonString;
    SimpleAdapter simpleAdapter;
    String data ;
    ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_home_screen);

        ArrayList<String> listOfURLs;
        //ArrayList<String> title_array;

        //title_array = new ArrayList<String>();
        listOfURLs = new ArrayList<String>();
        //listOfURLs.add(0,"http://stream3.nayatel.com/movies1/The.Martian-HD.mp4");


        listView = (ListView)findViewById(R.id.list1);
        SendHttpRequestTask t = new SendHttpRequestTask();
        String[] params = new String[]{url, data};
        t.execute(params);
        //listView.setAdapter(simpleAdapter);


        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        // login_username = prefs.getString("username", null);
        // login_password = prefs.getString("password", null);

        /*
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Welcome User");
        alert.setMessage("Username : " + login_username + "\n" + "Password : " + login_password + "\n");
        alert.setCancelable(true);
        alert.show();
        */




        setListAdapter(new ArrayAdapter<String>(this, R.layout.listview_home_screen, R.id.row , listOfURLs));
        listView = getListView();

       listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String urlName = parent.getItemAtPosition(position).toString();
                i = new Intent(HomeScreen.this,Videoview.class);
                startActivity(i);

                finish();
                Toast.makeText(HomeScreen.this,urlName,Toast.LENGTH_LONG).show();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu){

        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    List<Map<String,String>> employeeList = new ArrayList<Map<String,String>>();
    private List<Map<String, String>> initList(){

        try{
            //Object result = getJSONUrl(url);
            JSONObject jsonResponse = new JSONObject(jsonString);
            //String res = jsonResponse.getString("Movies");
            JSONArray jsonMainNode = jsonResponse.getJSONArray("Movies");//+jsonResponse.toString());// movieArray
            System.out.println("data json aRRAY [" + jsonMainNode.length() + "]");
            for(int i = 0; i<3;i++){

                JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
                //System.out.println("data json init list [" + jsonString + "]");
                //title_array.add(jsonChildNode.getString("emp_name").toString()); array of data
                // notice_array.add(jsonChildNode.getString("emp_no").toString()); array of data
                String name = jsonChildNode.getString("title");
                String number = jsonChildNode.getString("url");
                String outPut = name + "\n" +number;
                employeeList.add(createEmployee("Movies", outPut));

            }
            //Toast.makeText(getApplicationContext(), "Obj : " + jsonResponse.toString(), Toast.LENGTH_LONG).show();
        }
        catch(JSONException e){
            e.printStackTrace();
            System.out.println("ERROR  :" +e.toString());

            Toast.makeText(getApplicationContext(), "Error  :" + e.toString(), Toast.LENGTH_SHORT).show();
        }
        // adapter = new BaseAdapter2(MainActivity.this, title_array, notice_array);
        //list.setAdapter(adapter);
        return employeeList;
    }

    private HashMap<String, String> createEmployee(String name,String number){
        HashMap<String, String> employeeNameNo = new HashMap<String, String>();
        employeeNameNo.put(name, number);
        return employeeNameNo;
    }
    private class SendHttpRequestTask extends AsyncTask<String,Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String datasent=null;
            try {
                String url = params[0];
                String data = params[1];

                datasent = sendHttpRequest(url, data);
                System.out.println("data [" + datasent + "]");
                jsonString = datasent;
                System.out.println("data json [" + jsonString + "]");
                //Toast.makeText(getApplicationContext(), "json string :" + jsonString, Toast.LENGTH_LONG).show();

            }
            catch (Exception e){
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "ERROR :  "+e.toString(),Toast.LENGTH_LONG).show();
            }
            // System.out.println("URL ["+url+"] - NAME ["+name+"] -PASSWORD ["+password+"]");
            return jsonString;
        }



        @Override
        protected void onPostExecute(String result) {
            if(result.equals(null)){
                Toast.makeText(HomeScreen.this, "Response: " +result, Toast.LENGTH_LONG).show();

                System.out.println("Response result: " +result);

            }
            else{
                initList();
                simpleAdapter = new SimpleAdapter(HomeScreen.this, employeeList, R.layout.row_listitem, new String[] {"Movies"}, new int[] {R.id.txt_ttlsm_row});
                listView.setAdapter(simpleAdapter);
                System.out.println("Response result: " +result);
            }

        }
    }
    public String sendHttpRequest(String url, String data) {
        StringBuffer buffer=new StringBuffer();
        try {
            // Construct data
            //String data = "NAME" + "=" + URLEncoder.encode("sadaf", "UTF-8")+"&" + "PASSWORD" + "=" + URLEncoder.encode("0300", "UTF-8");
            HttpURLConnection conn = (HttpURLConnection) ( new URL(url)).openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.connect();
            // conn.getOutputStream().write( ("NAME=" + name).getBytes());

            //DataOutputStream out = new DataOutputStream(conn.getOutputStream());

            //   out.write(("DATA=" + data).getBytes());
          /*  for (int i=0;i<4;i++ ){
                out.writeUTF(data[i]);
            }*/
            //DataOutputStream out2 = new DataOutputStream(conn.getOutputStream());

            //out2.write(("PASSWORD=" + password).getBytes());

            InputStream is = conn.getInputStream();

            byte[] b = new byte[1024];

            while (is.read(b) != -1)
                buffer.append(new String(b));

       /*     while ((line = rd.readLine()) != null) {
                requestOutput.append(line); */
            conn.disconnect();
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return  buffer.toString();
    }

}
