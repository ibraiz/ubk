package com.ubroadkast.nayatel;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.media.MediaCodecInfo;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;


import com.wmspanel.libstream.Streamer;

import java.text.DecimalFormat;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p/>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends PreferenceActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener {
    private final String TAG = "SettingsActivity";
    /**
     * Determines whether to always show the simplified settings UI, where
     * settings are presented in a single list. When false, settings are shown
     * as a master/detail two-pane view on tablets. When true, a single pane is
     * shown on tablets.
     */

    private Streamer.CameraInfo init_cam_list() {

        Streamer.CameraInfo cameraInfo = null;

        ListPreference cam_list = (ListPreference) findPreference("cam_list");
        if(MainActivity.cameraList == null || MainActivity.cameraList.isEmpty()) {
            cam_list.setEnabled(false);
            cam_list.setSummary("No Camera Found:(");
        } else {
            cam_list.setEnabled(true);

            String[] cam_list_entries = new String[MainActivity.cameraList.size()];
            String[] cam_list_entry_values = new String[MainActivity.cameraList.size()];

            String cur_value = cam_list.getValue();
            boolean cur_value_found = false;

            for(int i = 0; i < MainActivity.cameraList.size(); i++) {
                Streamer.CameraInfo cam =  MainActivity.cameraList.get(i);
                String orienatation = cam.lensFacingBack ? "(BACK)" : "(FRONT)";
                cam_list_entries[i] = cam.cameraId + " " +  orienatation;
                cam_list_entry_values[i] = cam.cameraId;

                if(cam_list_entry_values[i].equals(cur_value)) {
                    cur_value_found = true;
                    cameraInfo = cam;
                }
            }

            cam_list.setEntries(cam_list_entries);
            cam_list.setEntryValues(cam_list_entry_values);

            if(!cur_value_found) {
                cam_list.setValue(cam_list_entry_values[0]);
                cameraInfo = MainActivity.cameraList.get(0);
            }

            cam_list.setSummary(cam_list.getEntry());
        }

        return cameraInfo;
    }

    private void init_bitarte(Streamer.CameraInfo cameraInfo) {
        EditTextPreference bitrate_edit = (EditTextPreference) findPreference("bitrate_edit");
        if(cameraInfo == null) {
            bitrate_edit.setEnabled(false);
        } else {
            bitrate_edit.setEnabled(true);
        }
        bitrate_edit.setSummary(bitrate_edit.getText());


    }

    private void init_video_size(Streamer.CameraInfo cameraInfo) {
        ListPreference video_size_list = (ListPreference) findPreference("video_size_list");
        if(cameraInfo == null) {
            video_size_list.setEnabled(false);
            video_size_list.setSummary(video_size_list.getEntry());
        } else {
            video_size_list.setEnabled(true);

            String[] video_size_list_entries = new String[cameraInfo.recordSizes.length];
            String[] video_size_list_entry_values = new String[cameraInfo.recordSizes.length];

            String cur_value = video_size_list.getValue();
            boolean cur_value_found = false;

            for(int i = 0; i < cameraInfo.recordSizes.length; i++) {
                video_size_list_entries[i] = cameraInfo.recordSizes[i].width + "x" + cameraInfo.recordSizes[i].height;
                video_size_list_entry_values[i] = Integer.toString(i);

                if(video_size_list_entry_values[i].equals(cur_value)) {
                    cur_value_found = true;
                }
            }

            video_size_list.setEntries(video_size_list_entries);
            video_size_list.setEntryValues(video_size_list_entry_values);

            if(!cur_value_found) {
                video_size_list.setValue(video_size_list_entry_values[0]);
            }

            video_size_list.setSummary(video_size_list.getEntry());
        }
    }

    private void init_fps(Streamer.CameraInfo cameraInfo) {
        ListPreference fps_range_list = (ListPreference) findPreference("fps_range_list");
        if (cameraInfo == null) {
            fps_range_list.setEnabled(false);
            fps_range_list.setSummary(fps_range_list.getEntry());
        } else {
            fps_range_list.setEnabled(true);

            String[] fps_range_list_entries = new String[cameraInfo.fpsRanges.length];
            String[] fps_range_list_entry_values = new String[cameraInfo.fpsRanges.length];

            String cur_value = fps_range_list.getValue();
            boolean cur_value_found = false;

            for (int i = 0; i < cameraInfo.fpsRanges.length; i++) {
                DecimalFormat df = new DecimalFormat("#0.0");

                if(cameraInfo.fpsRanges[i].fps_max >= 1000) {
                    fps_range_list_entries[i] = df.format(cameraInfo.fpsRanges[i].fps_min / 1000) + ".." + df.format(cameraInfo.fpsRanges[i].fps_max / 1000);
                } else {
                    fps_range_list_entries[i] = df.format(cameraInfo.fpsRanges[i].fps_min) + ".." + df.format(cameraInfo.fpsRanges[i].fps_max);
                }

                fps_range_list_entry_values[i] = Integer.toString(i);

                if (fps_range_list_entry_values[i].equals(cur_value)) {
                    cur_value_found = true;
                }
            }

            fps_range_list.setEntries(fps_range_list_entries);
            fps_range_list.setEntryValues(fps_range_list_entry_values);

            if (!cur_value_found) {
                fps_range_list.setValue(fps_range_list_entry_values[0]);
            }

            fps_range_list.setSummary(fps_range_list.getEntry());
        }
    }

    private void init_key_frame_interval(Streamer.CameraInfo cameraInfo) {
        EditTextPreference key_frame_interval_edit = (EditTextPreference) findPreference("key_frame_interval_edit");
        if(cameraInfo == null) {
            key_frame_interval_edit.setEnabled(false);
        } else {
            key_frame_interval_edit.setEnabled(true);
        }
        key_frame_interval_edit.setSummary(key_frame_interval_edit.getText());
    }

    private String getProfileName(int profile) {

        String profileName = "Unknown";

        switch(profile) {
            case MediaCodecInfo.CodecProfileLevel.AVCProfileBaseline:
                profileName = "Base";
                break;

            case MediaCodecInfo.CodecProfileLevel.AVCProfileMain:
                profileName = "Main";
                break;

            case MediaCodecInfo.CodecProfileLevel.AVCProfileExtended:
                profileName = "Extended";
                break;

            case MediaCodecInfo.CodecProfileLevel.AVCProfileHigh:
                profileName = "High";
                break;

            case MediaCodecInfo.CodecProfileLevel.AVCProfileHigh10:
                profileName = "High10";
                break;

            case MediaCodecInfo.CodecProfileLevel.AVCProfileHigh422:
                profileName = "High422";
                break;

            case MediaCodecInfo.CodecProfileLevel.AVCProfileHigh444:
                profileName = "High444";
                break;

            default:
                break;
        }
        return profileName;
    }

    private void init_video_profile() {
        ListPreference videoProfileList = (ListPreference) findPreference("video_profile");
        if(MainActivity.videoEncoder == null) {
            // auto
            videoProfileList.setValue("-1");
            videoProfileList.setSummary(videoProfileList.getEntry());
            return;
        }

        MediaCodecInfo.CodecProfileLevel[] profiles = MainActivity.videoEncoder.getSupportedProfiles();
        if(null == profiles) {
            // auto
            videoProfileList.setValue("-1");
            videoProfileList.setSummary(videoProfileList.getEntry());
            return;
        }

        String[] entries = new String[profiles.length + 1];
        String[] values  = new String[profiles.length + 1];

        String cur_value = videoProfileList.getValue();
        boolean cur_value_found = false;

        // add default item
        entries[0] = "Auto";
        values[0]  = "-1";

        // add supported profiles if exists
        for (int i = 0; i < profiles.length; i++) {
            entries[i + 1] = getProfileName(profiles[i].profile);
            values[i + 1] = Integer.toString(i);

            if (values[i + 1].equals(cur_value)) {
                cur_value_found = true;
            }
        }

        videoProfileList.setEntries(entries);
        videoProfileList.setEntryValues(values);

        if(!cur_value_found) {
            videoProfileList.setValue("-1");
        }

        videoProfileList.setSummary(videoProfileList.getEntry());
    }


    private void init_sample_rate() {
        ListPreference sample_rate_list = (ListPreference) findPreference("sample_rate_list");
        if(MainActivity.supportedSampleRates == null || MainActivity.supportedSampleRates.length == 0) {
            sample_rate_list.setSummary(sample_rate_list.getEntry());
        } else {

            String[] sample_rate_list_entries = new String[MainActivity.supportedSampleRates.length];
            String[] sample_rate_entry_values = new String[MainActivity.supportedSampleRates.length];

            String cur_value = sample_rate_list.getValue();
            boolean cur_value_found = false;

            for(int i = 0; i < MainActivity.supportedSampleRates.length; i++) {
                sample_rate_list_entries[i] = Integer.toString(MainActivity.supportedSampleRates[i]);
                sample_rate_entry_values[i] = Integer.toString(MainActivity.supportedSampleRates[i]);

                if(sample_rate_entry_values[i].equals(cur_value)) {
                    cur_value_found = true;
                }
            }

            sample_rate_list.setEntries(sample_rate_list_entries);
            sample_rate_list.setEntryValues(sample_rate_entry_values);

            if(!cur_value_found) {
                sample_rate_list.setValue(sample_rate_list_entries[0]);
            }

            sample_rate_list.setSummary(sample_rate_list.getEntry());
        }
    }

    private void init_channel_count() {

        ListPreference channel_count_list = (ListPreference) findPreference("channel_count_list");

        String[] channel_count_entries;
        String[] channel_count_entry_values;

        if(MainActivity.maxInputChannelCount > 1) {
            channel_count_entries = new String[] { "1", "2" };
            channel_count_entry_values = new String[] { "1", "2" };
        } else {
            channel_count_entries = new String[] { "1" };
            channel_count_entry_values = new String[] { "1" };
        }

        String cur_value = channel_count_list.getValue();
        boolean cur_value_found = false;

        for(String cursor : channel_count_entry_values) {
            if(cursor.equals(cur_value)) {
                cur_value_found = true;
                break;
            }
        }

        channel_count_list.setEntries(channel_count_entries);
        channel_count_list.setEntryValues(channel_count_entry_values);

        if(!cur_value_found) {
            channel_count_list.setValue(channel_count_entry_values[0]);
        }

        channel_count_list.setSummary(channel_count_list.getEntry());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref);

        // video settings
        Streamer.CameraInfo cameraInfo = init_cam_list();
        init_bitarte(cameraInfo);
        init_video_size(cameraInfo);
        init_fps(cameraInfo);
        init_key_frame_interval(cameraInfo);
        //init_video_profile();

        // audio settings
        init_sample_rate();
        init_channel_count();

        // connection setting
        ListPreference channel_count_list = (ListPreference) findPreference("channel_count_list");
        channel_count_list.setSummary(channel_count_list.getEntry());

        EditTextPreference conn0_uri = (EditTextPreference) findPreference("conn0_uri");
        conn0_uri.setSummary(conn0_uri.getText());

        ListPreference conn0_mode = (ListPreference) findPreference("conn0_mode");
        conn0_mode.setSummary(conn0_mode.getEntry());

      /*  EditTextPreference conn1_uri = (EditTextPreference) findPreference("conn1_uri");
        conn1_uri.setSummary(conn1_uri.getText());

        ListPreference conn1_mode = (ListPreference) findPreference("conn1_mode");
        conn1_mode.setSummary(conn1_mode.getEntry());

        EditTextPreference conn2_uri = (EditTextPreference) findPreference("conn2_uri");
        conn2_uri.setSummary(conn2_uri.getText());

        ListPreference conn2_mode = (ListPreference) findPreference("conn2_mode");
        conn2_mode.setSummary(conn2_mode.getEntry());
        */

        Preference about = (Preference)findPreference("about");
        about.setSummary("Version: " + BuildConfig.VERSION_NAME);
    }

    protected void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    protected void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }


    private void updateSummary(String key) {

        if(key.equals("cam_list")) {
            ListPreference cam_list = (ListPreference) findPreference("cam_list");
            cam_list.setSummary(cam_list.getEntry());

            String cameraId = cam_list.getValue();

            Streamer.CameraInfo cameraInfo = null;
            for (Streamer.CameraInfo cursor : MainActivity.cameraList) {
                if (cursor.cameraId.equals(cameraId)) {
                    cameraInfo = cursor;
                    break;
                }
            }

            init_video_size(cameraInfo);
            init_fps(cameraInfo);
            init_key_frame_interval(cameraInfo);
        } else if(key.equals("bitrate_edit")) {
            EditTextPreference bitrate_edit = (EditTextPreference) findPreference("bitrate_edit");
            bitrate_edit.setSummary(bitrate_edit.getText());
        } else if(key.equals("video_size_list")) {
            ListPreference video_size_list = (ListPreference) findPreference("video_size_list");
            video_size_list.setSummary(video_size_list.getEntry());
        } else if(key.equals("fps_range_list")) {
            ListPreference fps_range_list = (ListPreference) findPreference("fps_range_list");
            fps_range_list.setSummary(fps_range_list.getEntry());
        } else if(key.equals("key_frame_interval_edit")) {
            EditTextPreference key_frame_interval_edit = (EditTextPreference) findPreference("key_frame_interval_edit");
            key_frame_interval_edit.setSummary(key_frame_interval_edit.getText());
/*
        } else if(key.equals("video_profile")) {
            ListPreference profile = (ListPreference) findPreference("video_profile");
            profile.setSummary(profile.getEntry());
*/
        } else if(key.equals("sample_rate_list")) {
            ListPreference sample_rate_list = (ListPreference) findPreference("sample_rate_list");
            sample_rate_list.setSummary(sample_rate_list.getEntry());
        } else if(key.equals("channel_count_list")) {
            ListPreference channel_count_list = (ListPreference) findPreference("channel_count_list");
            channel_count_list.setSummary(channel_count_list.getEntry());
        } else if(key.equals("conn0_uri")) {
            EditTextPreference conn0_uri = (EditTextPreference) findPreference("conn0_uri");
            conn0_uri.setSummary(conn0_uri.getText());
        } else if(key.equals("conn0_mode")) {
            ListPreference conn0_mode = (ListPreference) findPreference("conn0_mode");
            conn0_mode.setSummary(conn0_mode.getEntry());
        } else if(key.equals("conn1_uri")) {
            EditTextPreference conn1_uri = (EditTextPreference) findPreference("conn1_uri");
            conn1_uri.setSummary(conn1_uri.getText());
        } else if(key.equals("conn1_mode")) {
            ListPreference conn1_mode = (ListPreference) findPreference("conn1_mode");
            conn1_mode.setSummary(conn1_mode.getEntry());
        } else if(key.equals("conn2_uri")) {
            EditTextPreference conn2_uri = (EditTextPreference) findPreference("conn2_uri");
            conn2_uri.setSummary(conn2_uri.getText());
        } else if(key.equals("conn2_mode")) {
            ListPreference conn2_mode = (ListPreference) findPreference("conn2_mode");
            conn2_mode.setSummary(conn2_mode.getEntry());
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.d(TAG, "onSharedPreferenceChanged key=" + key);
        updateSummary(key);
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        if(preference.getKey().equals("about")) {
            showAboutDialog();
        }

        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    private void showAboutDialog() {

        LayoutInflater layoutInflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View aboutView = layoutInflater.inflate(R.layout.about_us, null);

        TextView version = (TextView)aboutView.findViewById(R.id.version);
        version.setText("Version: " + BuildConfig.VERSION_NAME);

        TextView link = (TextView)aboutView.findViewById(R.id.promo);
        link.setMovementMethod(LinkMovementMethod.getInstance());

        AlertDialog.Builder aboutDialog = new AlertDialog.Builder(this)
                .setTitle(R.string.about)
                .setView(aboutView)
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setCancelable(false);

        aboutDialog.show();
    }
}
