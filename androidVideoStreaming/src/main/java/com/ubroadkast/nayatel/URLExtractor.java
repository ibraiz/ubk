package com.ubroadkast.nayatel;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class URLExtractor extends Activity {

    ArrayList<String> title_array = new ArrayList<String>();
    ArrayList<String> url_array = new ArrayList<String>();
    public static ArrayList title, notice;
    public ArrayList<String> items = new ArrayList<String>();
    public static String jsonString;
    ListView listView;
    SimpleAdapter simpleAdapter;
    String url = "http://ubroadkast.com/sample.php";
    String data ;
    ListView list;
    //BaseAdapter2 adapter;
    /*
    Integer[] imgid={
            R.drawable.button1,
            R.drawable.img,
            R.drawable.images,
            R.drawable.button1,
            R.drawable.images,
            R.drawable.img,
            R.drawable.images,
            R.drawable.img,
    };
    */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.list);
        SendHttpRequestTask t = new SendHttpRequestTask();
        String[] params = new String[]{url, data};
        t.execute(params);
        listView.setAdapter(simpleAdapter);
        //  CustomListAdapter adapter=new CustomListAdapter(this, this.populate(), imgid);

        // listView.setAdapter(adapter);
        //  SimpleAdapter simpleAdapter = new SimpleAdapter(this, employeeList, R.layout.row_listitem, new String[] {"employees"}, new int[] {R.id.txt_ttlsm_row});
        //SimpleAdapter simpleAdapter = new SimpleAdapter(this, this.populate(), R.layout.row_listitem, new String[] {"employees"}, new int[] {R.id.txt_ttlsm_row});
        // listView.setAdapter(simpleAdapter);
        // setListAdapter(new ArrayAdapter(
        //   this, android.R.layout.simple_list_item_1,
        //  this.populate()));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // selected item
                //  String product = ((TextView) view).getText().toString();

                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://stream1.ubroadkast.com:1935/03465777029/livestream/playlist.m3u8")));
                   /* TextView title2 = (TextView) view.findViewById(R.id.txt_ttlsm_row); // title
                    String song = title.get(position).toString();
                    title2.setText(song);


                    TextView title22 = (TextView) view.findViewById(R.id.txt_ttlcontact_row2); // notice
                    String song2 = notice.get(position).toString();
                    title22.setText(song2);*/
                // Launching new Activity on selecting single List Item
                //  Intent i = new Intent(getApplicationContext(), single_list_view.class);
                // sending data to new activity
                //  i.putExtra("product", product);
                //  startActivity(i);

            }
        });
    }




    List<Map<String,String>> employeeList = new ArrayList<Map<String,String>>();
    private void initList(){

        try{
            JSONObject jsonResponse = new JSONObject(jsonString);

            JSONArray jsonMainNode = jsonResponse.optJSONArray("Movies");//+jsonResponse.toString());// movieArray
            System.out.println("data json aRRAY [" + jsonMainNode.length() + "]");
            for(int i = 0; i<jsonMainNode.length();i++){

                JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
                System.out.println("data json init list [" + jsonString + "]");
                //title_array.add(jsonChildNode.getString("emp_name").toString()); array of data
                // notice_array.add(jsonChildNode.getString("emp_no").toString()); array of data
                String name = jsonChildNode.optString("title");
                String number = jsonChildNode.optString("url");
                String outPut = name + "\n" +number;
                employeeList.add(createEmployee("Movies", outPut));

            }

        }
        catch(JSONException e){
            Toast.makeText(getApplicationContext(), "Error" + e.toString(), Toast.LENGTH_SHORT).show();
        }
        // adapter = new BaseAdapter2(MainActivity.this, title_array, notice_array);
        //list.setAdapter(adapter);
    }

    private HashMap<String, String> createEmployee(String name,String number){
        HashMap<String, String> employeeNameNo = new HashMap<String, String>();
        employeeNameNo.put(name, number);
        return employeeNameNo;
    }
/*    private ArrayList<String> populate() {
        ArrayList<String> items = new ArrayList<String>();

        try {
            URL url = new URL
                    ("http://10.10.48.254/sql3.php");
            HttpURLConnection urlConnection =
                    (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.connect();
            Toast.makeText(getApplicationContext(), "lenght ", Toast.LENGTH_SHORT).show();
            // gets the server json data
            BufferedReader bufferedReader =
                    new BufferedReader(new InputStreamReader(
                            urlConnection.getInputStream()));
           // out.write(("DATA=" + data).getBytes());
           String next;
            while ((next = bufferedReader.readLine()) != null){
                JSONArray ja = new JSONArray(next);

                for (int i = 0; i < ja.length(); i++) {
                    JSONObject jo = (JSONObject) ja.get(i);
                    items.add(jo.getString("text"));
                    int num=items.toString().length();

                }
            }
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return items;
    }*/

    private class SendHttpRequestTask extends AsyncTask<String,Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String url = params[0];
            String data = params[1];

            String datasent = sendHttpRequest(url, data);
            System.out.println("data [" + datasent + "]");
            jsonString=datasent;
            System.out.println("data json [" + jsonString + "]");

            // System.out.println("URL ["+url+"] - NAME ["+name+"] -PASSWORD ["+password+"]");
            return datasent;
        }



        protected void onPostExecute(String result) {
            if(result.equals("")){
                // Toast.makeText(MainActivity.this, "Response: " +result, Toast.LENGTH_SHORT).show();

                System.out.println("Response result: " +result);

            }
            else{
                // i.putExtra("DATA",result);
                // startActivity(i);
            /*    String[] title;
                String  url;
               title= result.split("url");
                url=title[1];
                System.out.println("Response url: " +url);
                System.out.println("Response title: " +title[0]);*/
                initList();
                simpleAdapter = new SimpleAdapter(URLExtractor.this, employeeList, R.layout.row_listitem, new String[] {"Movies"}, new int[] {R.id.txt_ttlsm_row});
                // listView.setAdapter(simpleAdapter);
                //  CustomListAdapter adapter=new CustomListAdapter(MainActivity.this,result, imgid);

                //  listView.setAdapter(adapter);
                // Toast.makeText(MainActivity.this, "!!!Response: " +result, Toast.LENGTH_SHORT).show();
                System.out.println("Response result: " +result);
            }

        }
    }
    String sendHttpRequest(String url, String data) {
        StringBuffer buffer=new StringBuffer();
        try {
            // Construct data
            //String data = "NAME" + "=" + URLEncoder.encode("sadaf", "UTF-8")+"&" + "PASSWORD" + "=" + URLEncoder.encode("0300", "UTF-8");
            HttpURLConnection conn = (HttpURLConnection) ( new URL(url)).openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.connect();
            // conn.getOutputStream().write( ("NAME=" + name).getBytes());

            DataOutputStream out = new DataOutputStream(conn.getOutputStream());

            //   out.write(("DATA=" + data).getBytes());
          /*  for (int i=0;i<4;i++ ){
                out.writeUTF(data[i]);
            }*/
            //DataOutputStream out2 = new DataOutputStream(conn.getOutputStream());

            //out2.write(("PASSWORD=" + password).getBytes());

            InputStream is = conn.getInputStream();

            byte[] b = new byte[1024];

            while (is.read(b) != -1)
                buffer.append(new String(b));

       /*     while ((line = rd.readLine()) != null) {
                requestOutput.append(line); */
            conn.disconnect();
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return  buffer.toString();
    }


}
