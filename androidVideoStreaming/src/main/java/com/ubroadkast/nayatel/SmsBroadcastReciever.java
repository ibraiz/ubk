package com.ubroadkast.nayatel;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * Created by shakir on 2/29/2016.
 */


public class SmsBroadcastReciever extends BroadcastReceiver {
    private static final String LOG_TAG = "SMSBroadRec";
    public static final String SMS_BUNDLE = "pdus";
    String SenderNo  = "+923028555942";
    String SenderNo2 = "+923028555941";
    //Intent i;




    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d(LOG_TAG, "In onReceive()");

        Bundle bundle = intent.getExtras();
        String smsBody;
        String address;
        //String verificationCode;
        try {

            if ( !bundle.isEmpty() ){
                Log.d(LOG_TAG, "Sms received");
                //Toast.makeText(context ,"Sms should be received!", Toast.LENGTH_LONG).show();
                String verificationCode = null;
                Object[] sms = (Object[]) bundle.get(SMS_BUNDLE);
                for (Object sm : sms) {
                    SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) sm);
                    smsBody = smsMessage.getMessageBody();
                    address = smsMessage.getOriginatingAddress();
                    Log.d(LOG_TAG, address);
                    if (SenderNo.equals(address) || SenderNo2.equals(address)) {
                        verificationCode = getVerificationCode(smsBody);
                        clickthrurec(verificationCode);
                        Log.d(LOG_TAG, "OTP received: " + verificationCode);
                        break;
                    } else {
                        Log.d(LOG_TAG, "wrong sender");
                        break;
                    }
                }
                SharedPreferences prefs = context.getSharedPreferences(AppConfig.APP_SCRATCH, Context.MODE_PRIVATE);
                SharedPreferences.Editor ed = prefs.edit();
                ed.putString(AppConfig.APP_SCRATCH, verificationCode);
                if (ed.commit()) {
                    Log.d(LOG_TAG, "commit succesful");                          //added to the shared preferences
                } else {
                    Log.d(LOG_TAG, "commit unsuccessful");
                }
                /*
                login inst = login.instance();                          //this will update the UI with Scratchcode
                Log.d(LOG_TAG, String.valueOf(inst));
                inst.setThruRec(verificationCode);                      //function in login

                /*
                Intent broadcastIntent = new Intent();
                broadcastIntent.setAction("SMS_RECEIVED_ACTION");
                broadcastIntent.putExtra(AppConfig.APP_SCRATCH,verificationCode);
                context.sendBroadcast(broadcastIntent);
                */

            } else {
                Log.d(LOG_TAG, "Intent must be empty!");
                //Toast.makeText(context ,"No Sms received!", Toast.LENGTH_LONG).show();
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void clickthrurec (String ver){
        login inst = login.instance();                          //this will update the UI with Scratchcode
        Log.d(LOG_TAG, String.valueOf(inst));
        inst.setThruRec(ver);                                   //function in login

    }

    private String getVerificationCode(String message) {
        String code = null;
        int index = message.indexOf(AppConfig.OTP_DELIMETER);

        if (index != -1) {
            int start = index + 2;
            int length = 8;
            code = message.substring(start, start + length);
            return code;
        }
        return code;
    }
}
