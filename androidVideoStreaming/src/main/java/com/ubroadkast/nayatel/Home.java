package com.ubroadkast.nayatel;

import android.app.ActionBar;

import android.app.AlertDialog;

import android.app.FragmentTransaction;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;

import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;


import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.login.*;
import com.facebook.AccessTokenTracker;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareButton;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;



public class Home extends FragmentActivity implements ActionBar.TabListener {
    ActionBar actionbar;
    ViewPager view;
    String id;
    String name;
    String status;
    InputStream is=null;
    String result=null;
    String line=null;
    String responce;
    private Button fbShare;
    String livePublisher=AppConfig.APP_USER;
    static final int PICK_CONTACT_REQUEST = 1;  // The request code
    private Button saveBtn;
    private RadioGroup settingGroup;
    private RadioButton settingButton;
    private String streamPasscode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.activity_home);
     /*   ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse("http://www.nayatel.com/"))
                .build();


        ShareDialog.show(Home.this, content);
*/
        /*
        SQLiteDatabase mydatabase = openOrCreateDatabase("tbl_UBroadkast",MODE_PRIVATE,null);
        Cursor resultSet;
        resultSet = mydatabase.rawQuery("Select * from ubroadcastinfo",null);
        resultSet.moveToFirst();
        String info1 = resultSet.getString(0);
        String info2 = resultSet.getString(1);
        String info3=resultSet.getString(2);

*/



         settingsListerner();
        contactshareListener();

        AppConfig.STREAM_SHARE_CODE=generateShareCode();

        if(AppConfig.PUBLIC_CHECK.equals("0")) {

            ShareButton shareButton = (ShareButton) findViewById(R.id.f_share);
            ShareLinkContent content = new ShareLinkContent.Builder().setContentDescription("I am broadcasting live using U Broadkast android app. Stream passcode is " + AppConfig.STREAM_SHARE_CODE + ". Copyright 2015. Ubroadkast. All rights reserved.")
                    // ShareLinkContent content = new ShareLinkContent.Builder().setContentDescription("Stream passcode is "+AppConfig.STREAM_SHARE_CODE)
                    .setImageUrl(Uri.parse("http://ubroadkast.com/appServices/Capture.JPG"))
                    .setContentUrl(Uri.parse("http://ubroadkast.com/player/player/streamAuth.php"))
                    .build();
            shareButton.setShareContent(content);
        }else{
            ShareButton shareButton = (ShareButton) findViewById(R.id.f_share);
            ShareLinkContent content = new ShareLinkContent.Builder().setContentDescription("Public Link. Copyright 2015. Ubroadkast. All rights reserved.")
                    // ShareLinkContent content = new ShareLinkContent.Builder().setContentDescription("Stream passcode is "+AppConfig.STREAM_SHARE_CODE)
                    .setImageUrl(Uri.parse("http://ubroadkast.com/appServices/Capture.JPG"))
                    .setContentUrl(Uri.parse("http://ubroadkast.com/player/player/publicStream.php?id="+AppConfig.STREAM_SHARE_CODE))
                    .build();
            shareButton.setShareContent(content);
        }


        if(AppConfig.APP_SCRATCH.equals("")){


        LoginManager.getInstance().logOut();

        SQLiteDatabase mydatabase = openOrCreateDatabase("tbl_UBroadkast", MODE_PRIVATE, null);

        mydatabase.execSQL("UPDATE fblogins SET Status='Inactive',Identity='" +     AppConfig.APP_USER + "' WHERE Userid='AppUser' ");

changeIntent();


}





        SQLiteDatabase mydatabase = openOrCreateDatabase("tbl_UBroadkast",MODE_PRIVATE,null);
        Cursor resultSet;
        resultSet = mydatabase.rawQuery("Select * from fblogins",null);
        resultSet.moveToFirst();

        String status = resultSet.getString(1);

        mydatabase.execSQL("CREATE TABLE IF NOT EXISTS contact_shares(Phone VARCHAR);");
/*
        if(status.equals("Inactive")){
            Button fbbutton=(Button)findViewById(R.id.login_button);
            fbbutton.setVisibility(View.INVISIBLE);
        }
        else
        {
            Button fbbutton=(Button)findViewById(R.id.login_button);
            fbbutton.setVisibility(View.VISIBLE);
        }
*/
        resultSet = mydatabase.rawQuery("Select * from fblogins",null);
        resultSet.moveToFirst();
        String username = resultSet.getString(0);
        String fbstatus=resultSet.getString(1);
        String identity = resultSet.getString(2);


        if(fbstatus.equals("Active")) {
            livePublisher = identity;
        }


        final AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {

                if (currentAccessToken == null){
                    SQLiteDatabase mydatabase = openOrCreateDatabase("tbl_UBroadkast",MODE_PRIVATE,null);
                    mydatabase.execSQL("UPDATE fblogins SET Status='Inactive',Identity='Inactive' WHERE Userid='AppUser' ");
                    fbSignOffDB();
                  //  changeIntent();
                }

            }
        };


        Typeface customFont=Typeface.createFromAsset(getAssets(), "fonts/DJB Woke Up Late.ttf");

        actionbar= getActionBar();
      //  Drawable d=getResources().getDrawable(R.drawable.redmenu);
      //  actionbar.setBackgroundDrawable(d);


          actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        actionbar.addTab(actionbar.newTab().setText("Settings").setTabListener(this).setIcon(R.drawable.settingsred));
         actionbar.addTab(actionbar.newTab().setText("Live").setTabListener(this).setIcon(R.drawable.camred));

/*
        ////////////////////////////////////////// DIALOG //////////////////////////////////////////////////////////
        final Dialog dialog = new Dialog(Home.this);
        dialog.setContentView(R.layout.info_dialog);
        dialog.setTitle("Nayatel Live Broadcast Application");


        TextView text = (TextView) dialog.findViewById(R.id.text);
        text.setText("Welcome to Nayatel Live Broadcast,  ,Please select the live option from above to start broadcasting");
        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        image.setImageResource(R.drawable.ic_launcher);



     //   fbShareHandler();
        dialog.show();





        ////////////////////////////////////////// DIALOG //////////////////////////////////////////////////////////


        */


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        exitListener();


       final ToggleButton b = (ToggleButton) findViewById(R.id.camera_toggle);


        b.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if(b.isChecked()){

AppConfig.APP_CAMERA=1;

                }
                else{


                    AppConfig.APP_CAMERA=0;
            }

            }
        });



        final ToggleButton b1 = (ToggleButton) findViewById(R.id.flash_toggle);

if(AppConfig.APP_CAMERA==1){
    b.setChecked(true);
}
        else
{
    b.setChecked(false);
}


        if(AppConfig.APP_FLASH==true){
            b1.setChecked(true);
        }
        else
        {
            b1.setChecked(false);
        }

        b1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if(b1.isChecked()){

                    AppConfig.APP_FLASH=true;

                }
                else{


                    AppConfig.APP_FLASH=false;
                }

            }
        });



        final ToggleButton b3 = (ToggleButton) findViewById(R.id.toggle_privacy);


        if(AppConfig.PUBLIC_CHECK.equals("1")){
            b3.setChecked(true);
        }
        else
        {
            b3.setChecked(false);
        }
        b3.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if(b3.isChecked()){

                    AppConfig.PUBLIC_CHECK="1";
                    Toast.makeText(getBaseContext(), "Your stream will be public",
                            Toast.LENGTH_SHORT).show();
                }
                else{


                    AppConfig.PUBLIC_CHECK="0";
                    Toast.makeText(getBaseContext(), "Your stream will be private",
                            Toast.LENGTH_SHORT).show();

                }

            }
        });




    }
    public void exitListener(){

        Button app_exit=(Button) findViewById(R.id.app_exit);

        app_exit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);

            }
        });

    }
public void contactshareListener(){

    Button contactShareButton = (Button) findViewById(R.id.contact_share_btn);

    contactShareButton.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View v) {


            pickContact();

        }
    });
        }
public  void goLive(){
    Intent intent;
    intent = new Intent(this, MainActivity.class);
    finish();
    startActivity(intent);
}
public void settingsListerner(){

    saveBtn = (Button) findViewById(R.id.save_settings);
    settingGroup = (RadioGroup) findViewById(R.id.radioGroup1);

    saveBtn.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            // get selected radio button from radioGroup
            int selectedId = settingGroup.getCheckedRadioButtonId();

            // find the radiobutton by returned id
            settingButton = (RadioButton) findViewById(selectedId);

EditText newStreamname=(EditText)findViewById(R.id.newUserFirstName);
            AppConfig.STREAM_QUALITY=settingButton.getText().toString();
AppConfig.APP_STREAM_NAME=newStreamname.getText().toString();
            Toast.makeText(getBaseContext(), "Settings Saved",
                    Toast.LENGTH_SHORT).show();


            if(AppConfig.PUBLIC_CHECK.equals("0")) {

                ShareButton shareButton = (ShareButton) findViewById(R.id.f_share);
                ShareLinkContent content = new ShareLinkContent.Builder().setContentDescription("I am broadcasting live using U Broadkast android app. Stream passcode is " + AppConfig.STREAM_SHARE_CODE + ". Copyright 2015. Ubroadkast. All rights reserved.")
                        // ShareLinkContent content = new ShareLinkContent.Builder().setContentDescription("Stream passcode is "+AppConfig.STREAM_SHARE_CODE)
                        .setImageUrl(Uri.parse("http://ubroadkast.com/appServices/Capture.JPG"))
                        .setContentUrl(Uri.parse("http://ubroadkast.com/player/streamAuth.php"))
                        .build();
                shareButton.setShareContent(content);
            }else{
                ShareButton shareButton = (ShareButton) findViewById(R.id.f_share);
                ShareLinkContent content = new ShareLinkContent.Builder().setContentDescription("Public Link. Copyright 2015. Ubroadkast. All rights reserved.")
                        // ShareLinkContent content = new ShareLinkContent.Builder().setContentDescription("Stream passcode is "+AppConfig.STREAM_SHARE_CODE)
                        .setImageUrl(Uri.parse("http://ubroadkast.com/appServices/Capture.JPG"))
                        .setContentUrl(Uri.parse("http://ubroadkast.com/player/publicStream.php?id="+AppConfig.STREAM_SHARE_CODE))
                        .build();
                shareButton.setShareContent(content);
            }




            if(AppConfig.APP_STREAM_NAME.equals("")){

                AppConfig.NAME_CHECK="0";
            }else{
                AppConfig.NAME_CHECK="1";
            }


        }
    });


}





    @Override
    protected void onStop()
    {
/*
        LoginManager.getInstance().logOut();

        SQLiteDatabase mydatabase = openOrCreateDatabase("tbl_UBroadkast", MODE_PRIVATE, null);

        mydatabase.execSQL("UPDATE fblogins SET Status='Inactive',Identity='" +     AppConfig.APP_USER + "' WHERE Userid='AppUser' ");

*/


       



super.onStop();
    }


    @Override
      public void onBackPressed() {

   finish();
    }

    private void pickContact() {
        Intent pickContactIntent = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"));
        pickContactIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE); // Show user only contacts w/ phone numbers
        startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request it is that we're responding to
        if (requestCode == PICK_CONTACT_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                // Get the URI that points to the selected contact
                Uri contactUri = data.getData();
                // We only need the NUMBER column, because there will be only one row in the result
                String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER};

                // Perform the query on the contact to get the NUMBER column
                // We don't need a selection or sort order (there's only one result for the given URI)
                // CAUTION: The query() method should be called from a separate thread to avoid blocking
                // your app's UI thread. (For simplicity of the sample, this code doesn't do that.)
                // Consider using CursorLoader to perform the query.
                Cursor cursor = getContentResolver()
                        .query(contactUri, projection, null, null, null);
                cursor.moveToFirst();

                // Retrieve the phone number from the NUMBER column
                int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                String number = cursor.getString(column);

                Toast.makeText(getBaseContext(), "Contact Number Added To Share List",
                        Toast.LENGTH_SHORT).show();
              //   saveShareList(number.replaceAll("\\s+",""));
                String share_phone=number.replaceAll("\\s+","");

                SQLiteDatabase mydatabase = openOrCreateDatabase("tbl_UBroadkast",MODE_PRIVATE,null);



                mydatabase.execSQL("INSERT INTO contact_shares VALUES('"+share_phone+"');");
            }
        }
    }

   public void clearSettings(){

       ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

       nameValuePairs.add(new BasicNameValuePair("phoneno",livePublisher));



       try
       {
           HttpClient httpclient = new DefaultHttpClient();
           HttpPost httppost = new HttpPost("http://ubroadkast.com/appServices/clear_share.php");
           httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
           HttpResponse response = httpclient.execute(httppost);
           HttpEntity entity = response.getEntity();
           is = entity.getContent();
           Log.e("pass 1", "connection success ");
       }
       catch(Exception e)
       {
           Log.e("Fail 1", e.toString());
           Toast.makeText(getApplicationContext(), "Invalid IP Address",
                   Toast.LENGTH_LONG).show();
       }

       try
       {
           BufferedReader reader = new BufferedReader
                   (new InputStreamReader(is,"iso-8859-1"),8);
           StringBuilder sb = new StringBuilder();
           while ((line = reader.readLine()) != null)
           {
               sb.append(line + "\n");
           }
           is.close();
           result = sb.toString();
           Log.e("pass 2", "connection success ");
       }
       catch(Exception e)
       {
           Log.e("Fail 2", e.toString());
       }

       try
       {
           JSONObject json_data = new JSONObject(result);
           responce=(json_data.getString("responce"));
           Toast.makeText(getBaseContext(), "Contact Share List Cleared",
                   Toast.LENGTH_SHORT).show();
           Intent intent;
           if(responce.equals("1")){

           }
           else{

           }





       }
       catch(Exception e)
       {
           Log.e("Fail 3", e.toString());
       }

   }

    public void saveShareList(String share)
    {
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

        nameValuePairs.add(new BasicNameValuePair("phoneno",livePublisher));

        nameValuePairs.add(new BasicNameValuePair("share",share));

        try
        {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://ubroadkast.com/appServices/share_contacts.php");
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 1", e.toString());
            Toast.makeText(getApplicationContext(), "Invalid IP Address",
                    Toast.LENGTH_LONG).show();
        }

        try
        {
            BufferedReader reader = new BufferedReader
                    (new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 2", e.toString());
        }

        try
        {
            JSONObject json_data = new JSONObject(result);
            responce=(json_data.getString("responce"));

            Intent intent;
            if(responce.equals("1")){

            }
            else{

            }





        }
        catch(Exception e)
        {
            Log.e("Fail 3", e.toString());
        }
    }





    public void fbSignOffDB()
    {
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

        nameValuePairs.add(new BasicNameValuePair("phone",AppConfig.APP_USER));



        try
        {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://ubroadkast.com/appServices/checkFacebookSession.php");
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 1", e.toString());
            Toast.makeText(getApplicationContext(), "Invalid IP Address",
                    Toast.LENGTH_LONG).show();
        }

        try
        {
            BufferedReader reader = new BufferedReader
                    (new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 2", e.toString());
        }

        try
        {
            JSONObject json_data = new JSONObject(result);
            responce=(json_data.getString("responce"));

            Intent intent;
            if(responce.equals("1")){

            }
            else{

            }





        }
        catch(Exception e)
        {
            Log.e("Fail 3", e.toString());
        }
    }


/*
    public void fbShareHandler(){


        fbShare=(Button)findViewById(R.id.fbShare);
        fbShare.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

              //  Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ubroadcastlogo);


              //  SharePhoto photo = new SharePhoto.Builder()
                 //       .setBitmap(bm).build();
              //  SharePhotoContent content = new SharePhotoContent.Builder()
                    //    .addPhoto(photo)
                      //  .build();
                ShareLinkContent content = new ShareLinkContent.Builder().setContentDescription("Ubroadkast, Broadcast yourself live to unlimited audience on web")
                        .setContentUrl(Uri.parse("http://www.nayatel.com/"))
                       .build();

               ShareDialog.show(Home.this, content);

            }

        });
    }

*/



    public String generateShareCode(){

        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();





        try
        {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://ubroadkast.com/appServices/generate_sharecode.php");
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 1", e.toString());
            Toast.makeText(getApplicationContext(), "Invalid IP Address",
                    Toast.LENGTH_LONG).show();
        }

        try
        {
            BufferedReader reader = new BufferedReader
                    (new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 2", e.toString());
        }

        try
        {
            JSONObject json_data = new JSONObject(result);
            responce=(json_data.getString("responce"));
            String sharecode=(json_data.getString("sharecode"));


         return sharecode;

        }
        catch(Exception e)
        {
            Log.e("Fail 3", e.toString());
        }
return "";

    }


    public void changeIntent(){
        Intent intent;
        intent = new Intent(this, login.class);
        finish();
        startActivity(intent);
    }
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft){
        Intent intent;
        switch (tab.getPosition()) {

            case 0:


                break;
            case 1:

                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;



        }

    }


    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft){

        Intent intent;
        switch (tab.getPosition()) {

            case 0:


                break;
            case 1:

                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
            case 2:

                intent = new Intent(this, Settings.class);
                startActivity(intent);
                break;


        }




    }
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft){

    }
    private void doExit() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                Home.this);

        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
               clearSettings();
                finish();
            }
        });

        alertDialog.setNegativeButton("No", null);

        alertDialog.setMessage("Do you want to exit?");
        alertDialog.setTitle("Notice");
        alertDialog.show();
    }
    private void alert(final String msg) {

        AlertDialog.Builder builder = new AlertDialog.Builder(Home.this);
        builder.setMessage(msg).setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            pickContact();
            return true;
        }
        else if(id==R.id.action_clear)
        {
           clearSettings();
            SQLiteDatabase mydatabase = openOrCreateDatabase("tbl_UBroadkast",MODE_PRIVATE,null);

           mydatabase.execSQL("DELETE  FROM contact_shares");

            return true;
        }
        else if(id==R.id.action_feedback){

            Intent intent;
            intent = new Intent(this, rating.class);

            startActivity(intent);

            return true;
        }
        else if(id==R.id.action_version){


            AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
            builder.setTitle("App Info");
            builder.setMessage("U Broadkast Version 1.4 (Beta), Copyright © 2015, Nayatel (Pvt) Ltd. All rights reserved");
            builder.setPositiveButton("Ok", null);
            builder.setNegativeButton("Cancel", null);
            builder.show();




            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
