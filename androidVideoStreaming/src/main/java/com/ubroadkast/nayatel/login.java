package com.ubroadkast.nayatel;

import java.io.BufferedReader;

import java.io.InputStream;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.os.Handler;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;

import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.AccessTokenTracker;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import  android.content.DialogInterface;

public class login extends Activity {
    String id;
    String name;
    String status;
    InputStream is=null;
    String result=null;
    String line=null;
    String responce;
    private Button loginButton;
    private Button newUserBtn;
    ProgressDialog   progressDialog;
    private Button newRegisterBtn;
    private Button fblogin;
    boolean loginerror=false;
String scratch;
    private static login instance;
    public String getSimSerialNumber;





    CallbackManager callbackManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());


        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_login);
        callbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = (LoginButton)findViewById(R.id.login_button);
        //   loginButton.setVisibility(View.INVISIBLE);
        loginButton.setReadPermissions("user_friends");




        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String login_username = prefs.getString("username", "");
        String login_password = prefs.getString("password", ""); //return nothing if no pass saved
        CheckBox rememberMe = (CheckBox) findViewById(R.id.remember);
        if(login_password.equals("")){
            rememberMe.setChecked(false);
        }
        else{
            rememberMe.setChecked(true);
        }
        TextView phone=(TextView)findViewById(R.id.txtPhone);
        TextView password=(TextView)findViewById(R.id.txtPassword);
        phone.setText(login_username);
        password.setText(login_password);
        Context context = this;


        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();


        if(!(isConnected)){


            AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
            builder.setTitle("Network Error");
            builder.setMessage("Ooops! there seems to be a problem with your network connection. Please check your connection and try again");
            builder.setPositiveButton("Ok", null);
            builder.setNegativeButton("Cancel", null);


            builder.show();


            ////////////////////////////////////////// DIALOG //////////////////////////////////////////////////////////
          /*  final Dialog dialog = new Dialog(login.this);
            dialog.setContentView(R.layout.info_dialog);
            dialog.setTitle("Network Error");


            TextView text = (TextView) dialog.findViewById(R.id.text);
            text.setText("Ooops! there seems to be a problem with your network connection. Please check your connection and try again");
            ImageView image = (ImageView) dialog.findViewById(R.id.image);
            image.setImageResource(R.drawable.ubroadcastlogo);




            dialog.show(); */
            ////////////////////////////////////////// DIALOG //////////////////////////////////////////////////////////
            new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

                @Override
                public void run() {

                    finish();
                }
            }, 3000);

        }




        ////////////////////////////////////////// DIALOG //////////////////////////////////////////////////////////

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ContextThemeWrapper ctw = new ContextThemeWrapper( this,AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        //R.style.MyTheme );
        final AlertDialog.Builder builder= new AlertDialog.Builder( ctw );
        View layout = inflater.inflate(R.layout.instruction_dialog_custom, null);
        builder.setView(layout);
        builder.setOnCancelListener(null)
                .setCancelable(true);;
        // builder.setTitle("Instructions" )
        // .setIcon(android.R.drawable.ic_dialog_alert)
        // .setIcon(R.drawable.button_red)
        //    .setCancelable(false);
               /* builder.setMessage("Broadcast in 4 steps:\n" +
                        "1- Register free or Sign-in\n" +
                        "2- Share with contacts or Facebook\n" +
                        "3- Click on the LIVE tab\n" +
                        "4- Click red button to go live\n")
                ;
*/
        // .setIcon(android.R.drawable.ic_dialog_alert);
        builder.setPositiveButton("", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        builder.setCancelable(true);

                    }
                }
                // login.this.getDialog().cancel();
        );
        //  .setIcon(R.drawable.instruction_dialog_ok_btn);
        // builder.setNegativeButton("Cancel", null);
        // builder.setIcon(R.drawable.instruction_dialog_cancel_btn);
        builder.show();

        ////////////////////////////////////////// DIALOG //////////////////////////////////////////////////////////

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();



        StrictMode.setThreadPolicy(policy);
       // Context context;





        SQLiteDatabase mydatabase = openOrCreateDatabase("tbl_UBroadkast",MODE_PRIVATE,null);


        mydatabase.execSQL("CREATE TABLE IF NOT EXISTS fblogins(Userid VARCHAR,Status VARCHAR,Identity VARCHAR);");

        mydatabase.execSQL("CREATE TABLE IF NOT EXISTS ubroadcastinfo(Userid VARCHAR,Email VARCHAR,Status VARCHAR);");


        mydatabase.execSQL("INSERT INTO fblogins VALUES('AppUser','Inactive','Inactive');");


         Cursor resultSet;
         resultSet = mydatabase.rawQuery("Select * from fblogins",null);
         resultSet.moveToFirst();
         String username = resultSet.getString(0);
         String status = resultSet.getString(1);

        if(status.equals("Active")){
            changeIntent();
        }


        /**
        Cursor resultSet;
        resultSet = mydatabase.rawQuery("Select * from fblogins",null);
        resultSet.moveToFirst();
        String username = resultSet.getString(0);
        String status = resultSet.getString(1); */




      //  ImageView main1 = (ImageView)findViewById(R.id.imageView4);

        //Animation slideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slidedown);
        //main1.startAnimation(slideDown);
        resetPasswordHandler();
        loginEventHandler();
        SignupHandler();
/*
        callbackManager = CallbackManager.Factory.create();

        LoginButton loginButton = (LoginButton)findViewById(R.id.login_button);
        loginButton.setReadPermissions("user_friends");
*/
        AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {

                if (currentAccessToken == null) {
                    SQLiteDatabase mydatabase = openOrCreateDatabase("tbl_UBroadkast",MODE_PRIVATE,null);
                    mydatabase.execSQL("UPDATE fblogins SET Status='Inactive' WHERE Userid='AppUser' ");



                }
            }
        };




        facebookLogin();
       // facebookIntentHandler();
    }

    public void resetPasswordHandler() {

        Button resetpassword=(Button)findViewById(R.id.resetpassword);



        resetpassword.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                gotoResetPassword();


            }
        });

    }


    public void gotoResetPassword(){
        Intent intent;
        intent = new Intent(this, change_password.class);

        startActivity(intent);
    }
/*
    public void facebookIntentHandler(){
        Button goFacebook=(Button)findViewById(R.id.goFacebook);
        goFacebook.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                SQLiteDatabase mydatabase = openOrCreateDatabase("tbl_UBroadkast", MODE_PRIVATE, null);
                Cursor resultSet;
                resultSet = mydatabase.rawQuery("Select * from fblogins", null);
                resultSet.moveToFirst();
                String username = resultSet.getString(0);
                String status = resultSet.getString(1);

                if (status.equals("Active")) {
                    changeIntent();
                } else {


                    facebookIntentStart();
                }
            }

        });

}*/
public void facebookLogin(){


    callbackManager = CallbackManager.Factory.create();

    LoginButton loginButton = (LoginButton)findViewById(R.id.login_button);




    loginButton.setReadPermissions("user_friends");
    // Callback registration
    loginButton.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
            loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {


                    AccessToken accessToken = loginResult.getAccessToken();
                    TelephonyManager tel = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

                      /*  TelephonyManager telemamanger = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                 getSimSerialNumber = telemamanger.getSimSerialNumber();
                String getSimNumber = telemamanger.getLine1Number();

*/

                    getSimSerialNumber = tel.getDeviceId().toString();

                    Log.e("FB", String.valueOf(accessToken));
                    Profile profile = Profile.getCurrentProfile();


                    if (profile != null) {
                        //(String phone,String email,String firstname,String lastname,String loginMethod)
                        //  Register(profile.getId().toString()+"PID", "Not Available",  "FACEBOOK-ANDROID",getSimSerialNumber);
                        Register("FBAPP" + getSimSerialNumber, "Not Available", "FACEBOOK-ANDROID", getSimSerialNumber);
                    } else {
                        Register("FBAPP" + getSimSerialNumber, "Not Available", "FACEBOOK-ANDROID", getSimSerialNumber);
                    }
                    SQLiteDatabase mydatabase = openOrCreateDatabase("tbl_UBroadkast", MODE_PRIVATE, null);
                    AppConfig.APP_USER = accessToken.getUserId() + "(Facebook)";
                    mydatabase.execSQL("UPDATE fblogins SET Status='Active',Identity='" + AppConfig.APP_USER + "' WHERE Userid='AppUser' ");


                    changeIntent();
                }

                @Override
                public void onCancel() {

                }


                @Override
                public void onError(FacebookException exception) {
                    // App code
                }
            });

        }
    });
}
    public void termsIntent()
    {
        Intent intent;
        intent = new Intent(this, Terms.class);

        startActivity(intent);
    }
    public void changeIntent(){
        Intent intent;
        intent = new Intent(this, MainActivity.class);
        finish();
        startActivity(intent);
    }
    public void facebookIntentStart(){
        Intent intent;
        intent = new Intent(this, facebook.class);

        startActivity(intent);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {




        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    public void SignupHandler(){


    newUserBtn=(Button)findViewById(R.id.newUserDialog);
    newUserBtn.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View view) {


            signUpIntent();
        }
    });


}
    public void signUpIntent(){
        Intent intent;
        intent = new Intent(this, Signup.class);

        startActivity(intent);
    }
    public void loginEventHandler(){
    loginButton=(Button)findViewById(R.id.loginButton);
    loginButton.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            TextView phone = (TextView) findViewById(R.id.txtPhone);
            TextView password = (TextView) findViewById(R.id.txtPassword);

            if (phone.getText().toString().equals("") || password.getText().toString().equals("")) {

                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.custom_toast,
                        (ViewGroup) findViewById(R.id.toast_layout_root));

                TextView text = (TextView) layout.findViewById(R.id.text);
                text.setText("Username & Password Are Required");

                Toast toast = new Toast(getApplicationContext());
                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout);
                toast.show();
                return;

            }


            // select();

            new LongOperation().execute();


        }
    });
}

    public void Register(String phone,String email,String loginMethod,String imei)
    {
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();


        nameValuePairs.add(new BasicNameValuePair("phone",phone));
        nameValuePairs.add(new BasicNameValuePair("email",email));
        nameValuePairs.add(new BasicNameValuePair("imei",imei));

        nameValuePairs.add(new BasicNameValuePair("loginmethod",loginMethod));



        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast,
                (ViewGroup) findViewById(R.id.toast_layout_root));

        TextView text = (TextView) layout.findViewById(R.id.text);
        text.setText("Registering Account");

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
        try
        {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://ubroadkast.com/appServices/broadcastReg.php");
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 1", e.toString());
           // Toast.makeText(getApplicationContext(), "Invalid IP Address",
                  //  Toast.LENGTH_LONG).show();




        }

        try
        {
            BufferedReader reader = new BufferedReader
                    (new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 2", e.toString());
        }

        try
        {
            JSONObject json_data = new JSONObject(result);
            responce=(json_data.getString("responce"));
            scratch=(json_data.getString("scratch"));
            Intent intent;
            if(responce.equals("1")){



                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                SharedPreferences.Editor ed = prefs.edit();
                ed.putString("username", phone);
                ed.putString("password", scratch);
                ed.commit();
                Create_User_Wowza_App(phone,scratch,"UBROADKAST_FREE");
                AppConfig.APP_SCRATCH=scratch;
            }
            else if(responce.equals("2")){

            }
            else if(responce.equals("3")){

            }
            else{

            }





        }
        catch(Exception e)
        {
            Log.e("Fail 3", e.toString());
        }
    }
    public void Create_User_Wowza_App(String phone,String password, String version)
    {
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();


        nameValuePairs.add(new BasicNameValuePair("userid", phone));
        nameValuePairs.add(new BasicNameValuePair("password", password));
        nameValuePairs.add(new BasicNameValuePair("version", version));


        try
        {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://ubroadkast.com/appServices/Wowza-API/addFreeUbroadkastPlanApi.php");
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 1", e.toString());
         //   Toast.makeText(getApplicationContext(), "Invalid IP Address",
                   // Toast.LENGTH_LONG).show();
        }

        try
        {
            BufferedReader reader = new BufferedReader
                    (new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 2", e.toString());
        }

        try
        {
            JSONObject json_data = new JSONObject(result);
            responce=(json_data.getString("responce"));

            Intent intent;
            if(responce.equals("Assigned Successfully")){

                //   Toast.makeText(getApplicationContext(), "Success:"+responce,
                //     Toast.LENGTH_LONG).show();


            }
            else if (responce.equals("Already Exist")){


                // Toast.makeText(getApplicationContext(), "Success:"+responce,
                //Toast.LENGTH_LONG).show();
            }
            else {
                // Toast.makeText(getApplicationContext(), "ERROR:"+responce,
                // Toast.LENGTH_LONG).show();
            }







        }
        catch(Exception e)
        {
            Log.e("Fail 3", e.toString());
        }
    }
    public void select()
    {
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        TextView phone=(TextView)findViewById(R.id.txtPhone);
        TextView password=(TextView)findViewById(R.id.txtPassword);
        AppConfig.APP_USER=phone.getText().toString();
        nameValuePairs.add(new BasicNameValuePair("phone",phone.getText().toString()));
        nameValuePairs.add(new BasicNameValuePair("password", password.getText().toString()));




        try
        {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://ubroadkast.com/appServices/broadcastAuth.php");
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 1", e.toString());
          //  Toast.makeText(getApplicationContext(), "Invalid IP Address",
                 //   Toast.LENGTH_LONG).show();
        }

        try
        {
            BufferedReader reader = new BufferedReader
                    (new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 2", e.toString());
        }

        try
        {
            JSONObject json_data = new JSONObject(result);
            responce=(json_data.getString("responce"));

            Intent intent;
            if(responce.equals("1")){
                AppConfig.APP_USER=phone.getText().toString();
                intent = new Intent(this, MainActivity.class);
                AppConfig.APP_SCRATCH=password.getText().toString();
                loginerror=false;


                CheckBox rememberMe = (CheckBox) findViewById(R.id.remember);

                if (rememberMe.isChecked()) { //save username and pw to prefs
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                    SharedPreferences.Editor ed = prefs.edit();
                    ed.putString("username", AppConfig.APP_USER);
                    ed.putString("password", AppConfig.APP_SCRATCH);
                    if (ed.commit()) {
                        Log.e("login","commit succesful");
                    }
                    else {
                        Log.e("login","commit unsuccesful");

                    }

                }
                else{
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                    SharedPreferences.Editor ed = prefs.edit();
                    ed.putString("username", AppConfig.APP_USER);
                    ed.putString("password", password.getText().toString());
                    if (ed.commit()) {
                        Log.e("login","commit succesful");
                    }
                    else {
                        Log.e("login", "commit unsuccesful");
                    }

                }


                startActivity(intent);
            }
            else{
                loginerror=true;
                 AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                builder.setTitle("NOTICE");
                builder.setMessage("Your Login Information Seems To Be Incorrect, Please Check Your Credentials Again Or Contact Nayatel Support. +92 (51) 111 11 44 44   OR  99 from Nayatel landline");
                builder.setPositiveButton("Ok", null);
                builder.setNegativeButton("Cancel", null);
                builder.show();



            }





        }
        catch(Exception e)
        {
            Log.e("Fail 3", e.toString());
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void checkNetworkStrength() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo Info = cm.getActiveNetworkInfo();
        if (Info == null || !Info.isConnectedOrConnecting()) {

        } else {
            int netType = Info.getType();
            int netSubtype = Info.getSubtype();

            if (netType == ConnectivityManager.TYPE_WIFI) {

                WifiManager wifiManager = (WifiManager) getApplication().getSystemService(Context.WIFI_SERVICE);
                List<ScanResult> scanResult = wifiManager.getScanResults();
                for (int i = 0; i < scanResult.size(); i++) {
                    Log.d("scanResult", "Speed of wifi"+scanResult.get(i).level);//The db level of signal




                }


                // Need to get wifi strength
            } else if (netType == ConnectivityManager.TYPE_MOBILE) {

                // Need to get differentiate between 3G/GPRS
            }
        }
    }
    public static login instance(){
        return instance;
    }

    @Override
    public void onStart(){
        super.onStart();
        instance = this;
    }

    @Override
    public void onStop() {
        super.onStop();
        instance = null;
    }

    public void setThruRec(String verify){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String login_username = prefs.getString("username", AppConfig.APP_USER);

        //String login_password = prefs.getString("password", AppConfig.APP_SCRATCH);
        TextView username = (TextView) findViewById(R.id.txtPhone);
        TextView code = (TextView) findViewById(R.id.txtPassword);
        username.setText(login_username);
        code.setText(verify);
        loginButton=(Button)findViewById(R.id.loginButton);
        loginButton.performClick();
        Log.d("Login.java",login_username +"->"+verify);
        Log.d("Login.java", "setthrurec() called!");
    }

    private class LongOperation extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {


         select();


            return "executed";
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();

if(loginerror) {

    AlertDialog.Builder builder = new AlertDialog.Builder(login.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
    builder.setTitle("NOTICE");
    builder.setMessage("Your Login Information Seems To Be Incorrect, Please Check Your Credentials Again Or Contact Nayatel Support. +92 (51) 111 11 44 44   OR  99 from Nayatel landline");
    builder.setPositiveButton("Ok", null);
    builder.setNegativeButton("Cancel", null);
    builder.show();
}
        }

        @Override
        protected void onPreExecute() {

            progressDialog= ProgressDialog.show(login.this, "",
                    "Logging In. Please wait...", true);


        }

        @Override
        protected void onProgressUpdate(Void... values) {}
    }



}
