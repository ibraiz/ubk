package com.ubroadkast.nayatel;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.media.Image;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.provider.Telephony;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.wmspanel.libstream.AudioEncoder;
import com.wmspanel.libstream.Streamer;
import com.wmspanel.libstream.VideoEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.w3c.dom.Text;

import static android.widget.Toast.LENGTH_SHORT;

public class MainActivity extends Activity  implements SurfaceHolder.Callback, Streamer.Listener {
    private final String TAG = "MainActivity";

    private Handler mHandler;
    private Streamer mStreamer = null;

    public static List<Streamer.CameraInfo> cameraList;
    public static int[] supportedSampleRates = { 44100 };
    public static int maxInputChannelCount = 1;

    private String cameraId = "0";
    private Streamer.Size video_size = new Streamer.Size(640, 480);
    private int sampleRate = 44100;
    private int channelCount = 1;

    private AudioEncoder audioEncoder;
    public static VideoEncoder videoEncoder;
    private boolean shareFlag=false;

    private Timer uiUpateTimer;
    private TimerTask uiUpateTask;

    private Streamer.CONNECTION_STATE connectionState_;
    private long connTime_;
    private long prevTime_;
    private long prevBytes_;
    InputStream is=null;
    String result=null;
    String line=null;
    String responce;
    private Streamer.Size resolution;
    String selectedBitrate;
    String videoSetting="Very High";
boolean spinnerFlag;
    boolean privacyflag;
    String shareText;
    String shareLink;
    String ScratchCode;
    String cameraSwitch="0";
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "MainActivity: onStart(), orientation=" + getResources().getConfiguration().orientation);
    }

    private Streamer.CameraInfo getActiveCameraInfo(SharedPreferences sp) {

        Streamer.CameraInfo cameraInfo = null;

        if(cameraList == null || cameraList.size() == 0) {
            Log.e(TAG, "no camera found:(");
        } else {

            String cameraId = sp.getString("cam_list", "0");
            cameraId=cameraSwitch;
            for (Streamer.CameraInfo cursor : cameraList) {
                drawCameraInfo(cursor);
                if (cursor.cameraId.equals(cameraId)) {
                    cameraInfo = cursor;
                }
            }

            if (cameraInfo == null) {
                cameraInfo = cameraList.get(0);
            }
        }
        return cameraInfo;
    }

    Streamer.Size[] previewSizes;

    private void createVideoEncoder(SharedPreferences sp) {
        cameraId = "0";
        video_size = new Streamer.Size(640, 480);
        sampleRate = 44100;
        channelCount = 1;
        SharedPreferences shared = getSharedPreferences("MyPrefsFile", MODE_PRIVATE);

String SavedVideoSetting=shared.getString("videosetting","NULL");

        //Toast.makeText(MainActivity.this, SavedVideoSetting , Toast.LENGTH_LONG).show();

        if(SavedVideoSetting.toString().equals("Low")){
          //  Toast.makeText(MainActivity.this, "Inside Low " , Toast.LENGTH_LONG).show();

            resolution= new Streamer.Size(320,240);
            selectedBitrate="350000";
        }else if(SavedVideoSetting.toString().equals("Medium")){
            //Toast.makeText(MainActivity.this, "Inside Medium", Toast.LENGTH_LONG).show();
            resolution= new Streamer.Size(480,320);
            selectedBitrate="500000";
        }else if(SavedVideoSetting.toString().equals("High")){
            //Toast.makeText(MainActivity.this, "Inside High", Toast.LENGTH_LONG).show();
            resolution= new Streamer.Size(640,480);
            selectedBitrate="750000";
        }else if(SavedVideoSetting.toString().equals("Very High")){
           //Toast.makeText(MainActivity.this, "Inside Very High" , Toast.LENGTH_LONG).show();
            resolution= new Streamer.Size(1280,720);
            selectedBitrate="1500000";

        }else{
           //Toast.makeText(MainActivity.this, "No Comparison", Toast.LENGTH_LONG).show();
            resolution= new Streamer.Size(640,480);
            selectedBitrate="750000";
        }





        Streamer.CameraInfo cameraInfo = getActiveCameraInfo(sp);
        if(null != cameraInfo) {
            cameraId = cameraInfo.cameraId;
            if (cameraInfo.recordSizes != null && cameraInfo.recordSizes.length != 0) {
                String video_size_list = sp.getString("video_size_list", "0");
                int sizeIndex = Integer.valueOf(video_size_list);
                if (sizeIndex < 0 || sizeIndex >= cameraInfo.recordSizes.length) {
                    video_size = cameraInfo.recordSizes[0];
                } else {
                    video_size = cameraInfo.recordSizes[sizeIndex];
                }
            }
            previewSizes = cameraInfo.previewSizes;
        }

        videoEncoder = VideoEncoder.createVideoEncoder(resolution);
        //drawText("set video size =" + video_size.width + "x" + video_size.height);


        Streamer.FpsRange fpsRange = null;

        // frame rate
        String fps_range_list = sp.getString("fps_range_list", "0");
        Log.e(TAG, "createVideoEncoder: FPS Range:  "+fps_range_list);
        int fps_range_index = Integer.valueOf(fps_range_list);


        if(cameraInfo.fpsRanges != null && cameraInfo.fpsRanges.length != 0) {
            if(fps_range_index < 0 || fps_range_index >= cameraInfo.fpsRanges.length) {
                fpsRange = cameraInfo.fpsRanges[0];
            } else {
                fpsRange = cameraInfo.fpsRanges[fps_range_index];
            }
        }
        videoEncoder.setFrameRate(fpsRange);

        // video bitrate
        String bitrate_edit = sp.getString("bitrate_edit", "2000000");
        int bitRate = Integer.valueOf(bitrate_edit);
        videoEncoder.setBitRate(Integer.valueOf(selectedBitrate));
/*
        // video profile
        MediaCodecInfo.CodecProfileLevel[] profileLevel = videoEncoder.getSupportedProfiles();
        if (null != profileLevel && profileLevel.length > 0) {
            int profileIndex = Integer.valueOf(sp.getString("video_profile", "-1"));
            if(profileIndex >= 0 && profileIndex < profileLevel.length) {
                videoEncoder.setProfile(profileLevel[profileIndex]);
            }
        }
*/
        // video key frame interval
        int key_frame_interval = Integer.valueOf(sp.getString("key_frame_interval_edit", "2"));
        videoEncoder.setKeyFrameInterval(key_frame_interval);
    }

    private void createAudioEncoder(SharedPreferences sp) {
        // audio sample rate
        audioEncoder = AudioEncoder.createAudioEncoder();
        supportedSampleRates = audioEncoder.getSupportedSampleRates();
        if(supportedSampleRates != null) {
            sampleRate = Integer.valueOf(sp.getString("sample_rate_list", "44100"));
            boolean found = false;
            for(int cursor : supportedSampleRates) {
                if(cursor == sampleRate) {
                    found = true;
                    break;
                }
            }

            if(!found) {
                sampleRate = supportedSampleRates[0];
            }
            audioEncoder.setSampleRate(sampleRate);
            //drawText("set sampleRate=" + sampleRate);
        }

        // audio channel count
        maxInputChannelCount = audioEncoder.getMaxInputChannelCount();
        if(maxInputChannelCount != 1) {
            channelCount = Integer.valueOf(sp.getString("channel_count_list", "1"));
        }
        audioEncoder.setChannelCount(channelCount);
        //drawText("set channel count=" + channelCount);

        // audio profile
        audioEncoder.setAACProfile(MediaCodecInfo.CodecProfileLevel.AACObjectLC);

        // audio bitarte
        int audioBitRate = AudioEncoder.calcBitRate(sampleRate, channelCount, MediaCodecInfo.CodecProfileLevel.AACObjectLC);
        audioEncoder.setBitRate(audioBitRate);
        //drawText("set audio bitrate=" + audioBitRate);
    }


    private void createStreamer() {
        if(mStreamer == null) {
            mStreamer = new Streamer();
            mStreamer.setUserAgent("Ubroadkast" + "/" + BuildConfig.VERSION_NAME + "-" + BuildConfig.VERSION_CODE);
            cameraList = mStreamer.getCameraList(this);
        }
    }

    private void releaseStreamer() {
        broadcastEnable = false;

        if(null != mStreamer) {

            releaseConnection();

            mStreamer.stopAudioCapture();
            mStreamer.stopVideoCapture();

            if (null != videoEncoder) {
                videoEncoder.release();
                videoEncoder = null;
            }

            if (null != audioEncoder) {
                audioEncoder.release();
                audioEncoder = null;
            }

            mStreamer.release();
            mStreamer = null;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

      resumeLogic();

    }
public void resumeLogic(){
    Log.d(TAG, "MainActivity: onResume(), orientation=" + getResources().getConfiguration().orientation);

    if(getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
        return;
    }

/** Resume the connection */
    connectionId_ = -1;
    connectionState_ = Streamer.CONNECTION_STATE.DISCONNECTED;
    setConnectionStatus(Color.GREEN, "");

    createStreamer();

    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
    createVideoEncoder(sp);
    createAudioEncoder(sp);
/** Resume the connection */
    FacebookSdk.sdkInitialize(getApplicationContext());
    setContentView(R.layout.activity_main);








    getShareCodeTask t = new getShareCodeTask();
    String[] params = new String[]{""};
    t.execute(params);

    ImageView live_mark=(ImageView)findViewById(R.id.live_mark);
    live_mark.setVisibility(View.INVISIBLE);
spinnerFlag=false;
privacyflag=false;



    // Spinner element
    final Spinner spinner = (Spinner) findViewById(R.id.spinner);
    final Spinner privacy_spinner=(Spinner)findViewById(R.id.privacy_spinner);


    SharedPreferences shared = getSharedPreferences("MyPrefsFile", MODE_PRIVATE);

    String selectedSetting=shared.getString("videosetting", "NULL");
    String selectedPrivacySetting=shared.getString("privacy","NULL");
   // Toast.makeText(MainActivity.this,(selectedSetting),
     //       Toast.LENGTH_SHORT).show();
    // Spinner click listener


    // Spinner Drop down elements
    List<String> categories = new ArrayList<String>();

    categories.add("Low");
    categories.add("Medium");
    categories.add("High");
    categories.add("Very High");

    List<String> privacyOptions = new ArrayList<String>();

    privacyOptions.add("Public");
    privacyOptions.add("Private");



    // Creating adapter for spinner
    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.empty_spinner, categories);

    // Drop down layout style - list view with radio button
    dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown);


    // attaching data adapter to spinner
    spinner.setAdapter(dataAdapter);



    // Creating adapter for spinner
    ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this, R.layout.empty_privacy_spinner, privacyOptions);

    // Drop down layout style - list view with radio button
    dataAdapter2.setDropDownViewResource(R.layout.spinner_privacy_dropdown);


    // attaching data adapter to spinner
    privacy_spinner.setAdapter(dataAdapter2);


    privacy_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            // On selecting a spinner item
            String item = parentView.getItemAtPosition(position).toString();

            // Showing selected spinner item
            //  Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();

            SharedPreferences shared = getSharedPreferences("MyPrefsFile", MODE_PRIVATE);
            SharedPreferences.Editor editor = shared.edit();

            Button capture_button = (Button) findViewById(R.id.capture_button);

            if (privacyflag) {
                if (item.equals("Public")) {
                    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);


                    String username = sp.getString("username", "DEFAULT");
                    String password = sp.getString("password", "DEFAULT");
                    shareText = "Live Broadkasting With Ubroadkast Android App, Nayatel PVT LTD";
                    shareLink = "Watch me live at http://ubroadkast.com/player/streamview.php?id=" + username;

                    editor.putString("privacy", "Public");
                    editor.commit();

                } else if (item.equals("Private")) {


                    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);


                    String username = sp.getString("username", "DEFAULT");
                    String password = sp.getString("password", "DEFAULT");
                    insert("livestream", username, "STOPPED", "ANDROID", "rtsp://" + username + ":" + password + "@stream1.ubroadkast.com:1935/" + username + "/livestream", "NULL", ScratchCode);

                    shareText = "Live Broadkasting With Ubroadkast Android App, Nayatel PVT LTD";
                    shareLink = "Watch me live at http://ubroadkast.com/player/streamAuth.php" + "   I am broadkasting live using Ubroadkast android app, Password : " + ScratchCode;

                    editor.putString("privacy", "Private");
                    editor.commit();

                }
            } else {

                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);


                String username = sp.getString("username", "DEFAULT");
                String password = sp.getString("password", "DEFAULT");
                shareText = "Live Broadkasting With Ubroadkast Android App, Nayatel PVT LTD";
                shareLink = "Watch me live at http://ubroadkast.com/player/streamview.php?id=" + username;




                privacyflag = true;
            }

        }

        @Override
        public void onNothingSelected(AdapterView<?> parentView) {
            // your code here
        }

    });





    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            // On selecting a spinner item
            String item = parentView.getItemAtPosition(position).toString();

            // Showing selected spinner item
            //  Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();

            SharedPreferences shared = getSharedPreferences("MyPrefsFile", MODE_PRIVATE);
            SharedPreferences.Editor editor = shared.edit();

            Button capture_button = (Button) findViewById(R.id.capture_button);

if(spinnerFlag){
            if (item.equals("Low")) {

                videoSetting = "Low";
                editor.putString("videosetting", videoSetting);
                editor.commit();
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);

                Log.d(TAG, "MainActivity: onStop(), orientation=" + getResources().getConfiguration().orientation);

                Log.d(TAG, "MainActivity: onStop(), Activity Stoppped");

                releaseStreamer();
                resumeLogic();


            } else if (item.equals("Medium")) {

                videoSetting = "Medium";

                editor.putString("videosetting", videoSetting);
                editor.commit();
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                Log.d(TAG, "MainActivity: onStop(), orientation=" + getResources().getConfiguration().orientation);

                Log.d(TAG, "MainActivity: onStop(), Activity Stoppped");

                releaseStreamer();
                resumeLogic();


            } else if (item.equals("High")) {
                videoSetting = "High";
                editor.putString("videosetting", videoSetting);
                editor.commit();
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                Log.d(TAG, "MainActivity: onStop(), orientation=" + getResources().getConfiguration().orientation);

                Log.d(TAG, "MainActivity: onStop(), Activity Stoppped");

                releaseStreamer();
                resumeLogic();


            } else if (item.equals("Very High")) {

                videoSetting = "Very High";
                editor.putString("videosetting", videoSetting);
                editor.commit();
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                Log.d(TAG, "MainActivity: onStop(), orientation=" + getResources().getConfiguration().orientation);

                Log.d(TAG, "MainActivity: onStop(), Activity Stoppped");

                releaseStreamer();
                resumeLogic();


            } else {

            }

        }else{
    spinnerFlag=true;
        }

        }

        @Override
        public void onNothingSelected(AdapterView<?> parentView) {
            // your code here
        }

    });



        if(selectedSetting.equals("Low")){
            spinner.setSelection(0);
        }else if(selectedSetting.equals("Medium")){
            spinner.setSelection(1);
        }else if(selectedSetting.equals("High")){
            spinner.setSelection(2);
        }else if(selectedSetting.equals("Very High")){
            spinner.setSelection(3);
        }else if(selectedSetting.equals("NULL")){
            spinner.setSelection(2);
        }


    if(selectedPrivacySetting.equals("Public")){

        privacy_spinner.setSelection(0);
    }else if(selectedPrivacySetting.equals("Private")){

        privacy_spinner.setSelection(1);

    }else if(selectedSetting.equals("NULL")){

        privacy_spinner.setSelection(0);
    }

    final Button camera_switch=(Button) findViewById(R.id.camera_switch);

    camera_switch.setOnClickListener
            (
                    new View.OnClickListener() {
                        public void onClick(View v) {


                            if (cameraSwitch.equals("0")) {

                                cameraSwitch = "1";
                                releaseStreamer();
                                resumeLogic();


                                if(AppConfig.isLive.equals("1")) {
                                    Button capture_button = (Button) findViewById(R.id.capture_button);
                                    //  if (!broadcastEnable) {
                                    broadcastEnable = false;
                                    if (mStreamer != null) {
                                        releaseConnection();

                                        mStreamer.stopAudioCapture();
                                        mStreamer.stopVideoCapture();
                                    }
                                    broadcastEnable = true;
                                    Drawable d = getResources().getDrawable(R.drawable.upressed);
                                    capture_button.setBackground(d);
                                    ImageView live_mark = (ImageView) findViewById(R.id.live_mark);
                                    Animation animation;
                                    animation = AnimationUtils.loadAnimation(getApplicationContext(),
                                            R.anim.blink);
                                    live_mark.startAnimation(animation);
                                    live_mark.setVisibility(View.VISIBLE);
                                    capture_button.setText("");

                                    createConnection();
                                }
                            } else if (cameraSwitch.equals("1")) {

                                cameraSwitch = "0";
                                releaseStreamer();
                                resumeLogic();
                                if(AppConfig.isLive.equals("1")) {
                                    Button capture_button = (Button) findViewById(R.id.capture_button);
                                    //  if (!broadcastEnable) {
                                    broadcastEnable = false;
                                    if (mStreamer != null) {
                                        releaseConnection();

                                        mStreamer.stopAudioCapture();
                                        mStreamer.stopVideoCapture();
                                    }
                                    broadcastEnable = true;
                                    Drawable d = getResources().getDrawable(R.drawable.upressed);
                                    capture_button.setBackground(d);
                                    ImageView live_mark = (ImageView) findViewById(R.id.live_mark);
                                    Animation animation;
                                    animation = AnimationUtils.loadAnimation(getApplicationContext(),
                                            R.anim.blink);
                                    live_mark.startAnimation(animation);
                                    live_mark.setVisibility(View.VISIBLE);
                                    capture_button.setText("");

                                    createConnection();
                                }
                            }


                        }
                    });
/** Resgister surface view holder */
    SurfaceView surfaceView = (SurfaceView)findViewById(R.id.surfaceView);
    SurfaceHolder surfaceHolder = surfaceView.getHolder();
    surfaceHolder.addCallback(this);
    /** Register surface view holder */
    Button share_button=(Button) findViewById(R.id.share_button);

    share_button.setOnClickListener
            (
                    new View.OnClickListener() {
                        public void onClick(View v) {
                            if(!broadcastEnable){
                                LayoutInflater inflater = getLayoutInflater();
                                View layout = inflater.inflate(R.layout.custom_toast,
                                        (ViewGroup) findViewById(R.id.toast_layout_root));

                                TextView text = (TextView) layout.findViewById(R.id.text);
                                text.setText("Hit the broadkast button first");

                                Toast toast = new Toast(getApplicationContext());
                                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                toast.setDuration(Toast.LENGTH_LONG);
                                toast.setView(layout);
                                toast.show();
                                return ;
                            }

                               /* LayoutInflater factory = LayoutInflater.from(MainActivity.this);
                                final View deleteDialogView = factory.inflate(
                                        R.layout.share_box, null);
                                final AlertDialog deleteDialog = new AlertDialog.Builder(MainActivity.this).create();
                                deleteDialog.setView(deleteDialogView);*/
                            /*    deleteDialogView.findViewById(R.id.yes).setOnClickListener(new View.OnClickListener() {

                                    @Override
                                    public void onClick(View v) {
                                        //your business logic
                                        deleteDialog.dismiss();
                                    }
                                });
                                deleteDialogView.findViewById(R.id.no).setOnClickListener(new OnClickListener() {

                                    @Override
                                    public void onClick(View v) {
                                        deleteDialog.dismiss();

                                    }
                                });*/

                            //deleteDialog.show();
                            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);


                            String username = sp.getString("username", "DEFAULT");
                            String password = sp.getString("password", "DEFAULT");

                           /* Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);

                            sharingIntent.setType("text/plain");
                            String shareBody = shareLink;
                            sharingIntent.putExtra(Intent.EXTRA_TITLE, "Ubroadkast By Nayatel");
                            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, shareLink);
                            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                            startActivity(Intent.createChooser(sharingIntent, "Share via"));
*/


                         //   initShareIntent("mail");

                            List<Intent> targetedShareIntents = new ArrayList<Intent>();

                            Intent intent = new Intent(Intent.ACTION_SEND);
                            intent.setType("text/plain");
                            intent.putExtra(Intent.EXTRA_TEXT, shareLink);

                            List<ResolveInfo> resInfo = MainActivity.this.getPackageManager().queryIntentActivities(intent, 0);

                            for (ResolveInfo resolveInfo : resInfo) {
                                String packageName = resolveInfo.activityInfo.packageName;

                                Intent targetedShareIntent = new Intent(android.content.Intent.ACTION_SEND);
                                targetedShareIntent.setType("text/plain");
                                targetedShareIntent.putExtra(Intent.EXTRA_TEXT, shareLink);
                                targetedShareIntent.setPackage(packageName);
                                String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(MainActivity.this);
                                if (packageName.equals("com.facebook.katana") || packageName.equals("com.google.android.gm") || packageName.equals("com.whatsapp") || packageName.equals((defaultSmsPackageName)) || packageName.equals(("com.twitter.android")) || packageName.equals(("com.google.android.apps.plus")) || packageName.equals(("com.facebook.orca"))) { //com.facebook.orca Add Intent share
                                    targetedShareIntents.add(targetedShareIntent);
                                }
                            }
// Add my own activity in the share Intent chooser


                            Intent chooserIntent = Intent.createChooser(
                                    targetedShareIntents.remove(0), "Share Your Stream");

                            chooserIntent.putExtra(
                                    Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));

                            startActivity(chooserIntent);
                            shareFlag=true;
                        }
                    });
    final Button open_settings=(Button)findViewById(R.id.open_settings);
    final LinearLayout propLayout = (LinearLayout) findViewById(R.id.Settings_Area);
    final LinearLayout area1 = (LinearLayout) findViewById(R.id.linearLayout2);
    propLayout.setVisibility(View.INVISIBLE);

       /* Animation animation;
        animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fade_out);
        propLayout.startAnimation(animation);
        propLayout.setVisibility(View.INVISIBLE);
        Button flash_button=(Button) findViewById(R.id.flash_button);
        Button setting_button=(Button) findViewById(R.id.settings_button);
        Button share_button=(Button) findViewById(R.id.share_button);

        flash_button.setEnabled(false);
        setting_button.setEnabled(false);
        share_button.setEnabled(false);*/
    open_settings.setOnClickListener
            (
                    new View.OnClickListener() {
                        public void onClick(View v) {
                            if (propLayout.getVisibility() == View.VISIBLE) {
                                Animation animation;
                                animation = AnimationUtils.loadAnimation(getApplicationContext(),
                                        R.anim.slideleft);
                                area1.setBackgroundColor(Color.TRANSPARENT);

                                propLayout.startAnimation(animation);

                                propLayout.setVisibility(View.INVISIBLE);

                                Drawable settingstate=getResources().getDrawable(R.drawable.setting);
                                open_settings.setBackground(settingstate);
                                Button flash_button=(Button) findViewById(R.id.flash_button);
                                Button setting_button=(Button) findViewById(R.id.settings_button);
                                Button share_button=(Button) findViewById(R.id.share_button);
                                Spinner privacy_spinner=(Spinner) findViewById(R.id.privacy_spinner);
                                Button show_info=(Button) findViewById(R.id.show_info);
                                Button log_out=(Button) findViewById(R.id.logout);
log_out.setEnabled(false);
                                flash_button.setEnabled(false);
                                setting_button.setEnabled(false);
                                setting_button.setEnabled(false);
                                show_info.setEnabled(false);
                                privacy_spinner.setEnabled(false);

                            } else {
                                Animation animation;
                                animation = AnimationUtils.loadAnimation(getApplicationContext(),
                                        R.anim.slide_right);
                                propLayout.startAnimation(animation);
                                area1.setAnimation(animation);
                                propLayout.setVisibility(View.VISIBLE);
                                Drawable d = getResources().getDrawable(R.drawable.bakbar);
                                area1.setBackground(d);
                                Drawable settingstate=getResources().getDrawable(R.drawable.settingselected);
                                open_settings.setBackground(settingstate);
                                Button flash_button=(Button) findViewById(R.id.flash_button);
                                Button setting_button=(Button) findViewById(R.id.settings_button);
                                Button share_button=(Button) findViewById(R.id.share_button);
                                Spinner privacy_spinner=(Spinner) findViewById(R.id.privacy_spinner);
                                Button show_info=(Button) findViewById(R.id.show_info);
                                Button log_out=(Button) findViewById(R.id.logout);
                                log_out.setEnabled(true);

                                flash_button.setEnabled(true);
                                setting_button.setEnabled(true);
                                setting_button.setEnabled(true);
                                show_info.setEnabled(true);
                                privacy_spinner.setEnabled(true);
                            }
                        }
                    }
            );
    TextView con_status=(TextView)findViewById(R.id.connectionStatus);
    TextView fps_status=(TextView)findViewById(R.id.fpsView);

    con_status.setVisibility(View.INVISIBLE);
    fps_status.setVisibility(View.INVISIBLE);

    final Button show_info = (Button) findViewById(R.id.show_info);
    show_info.setOnClickListener(new View.OnClickListener() {
        public void onClick(View v) {



            TextView con_status=(TextView)findViewById(R.id.connectionStatus);
            TextView fps_status=(TextView)findViewById(R.id.fpsView);

            if(con_status.getVisibility() == View.VISIBLE && fps_status.getVisibility()==View.VISIBLE){
                con_status.setVisibility(View.INVISIBLE);
                fps_status.setVisibility(View.INVISIBLE);
            }else{
                con_status.setVisibility(View.VISIBLE);
                fps_status.setVisibility(View.VISIBLE);
            }

        }
    });
    if(shareFlag){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast,
                (ViewGroup) findViewById(R.id.toast_layout_root));

        TextView text = (TextView) layout.findViewById(R.id.text);
        text.setText("Resuming Broadkast");

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();


        Animation animation;
        animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.blink);
        live_mark.startAnimation(animation);
        live_mark.setVisibility(View.VISIBLE);

        Button capture_button=(Button)findViewById(R.id.capture_button);

        //  if (!broadcastEnable) {
        broadcastEnable = false;
        if(mStreamer != null ) {
            releaseConnection();

            mStreamer.stopAudioCapture();
            mStreamer.stopVideoCapture();
        }
        broadcastEnable = true;
        capture_button.setText("");
        Drawable d = getResources().getDrawable(R.drawable.upressed);
        capture_button.setBackground(d);
        //drawText("connection enabled");

        //  enableSettingsButton(false);

        createConnection();

           /* } else {
                broadcastEnable = false;
                capture_button.setText("Broadcast");

                // enableSettingsButton(true);

                if (-1 != connectionId_) {
                    mStreamer.releaseConnection(connectionId_);
                }
            }*/


        shareFlag=false;


    }/*
        else
       {
            broadcastEnable = false;
            Button capture_button=(Button)findViewById(R.id.capture_button);
            capture_button.setText("Broadcast");

            // enableSettingsButton(true);

            if (-1 != connectionId_) {
                mStreamer.releaseConnection(connectionId_);
            }
        }*/
    final Button log_out = (Button) findViewById(R.id.logout);
    log_out.setOnClickListener(new View.OnClickListener() {
        public void onClick(View v) {


            LoginManager.getInstance().logOut();
            final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);

            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);

            sp.edit().clear().commit();
            prefs.edit().clear().commit();
            startActivity(new Intent(getApplicationContext(), login.class));

            finish();

        }
    });
    final Button capture_button = (Button) findViewById(R.id.capture_button);
    capture_button.setOnClickListener(new View.OnClickListener() {
        public void onClick(View v) {
            // Perform action on click

            if (!broadcastEnable) {
                broadcastEnable = true;
                AppConfig.isLive="1";
                Drawable d = getResources().getDrawable(R.drawable.upressed);
                capture_button.setBackground(d);
                ImageView live_mark=(ImageView)findViewById(R.id.live_mark);
                Animation animation;
                animation = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.blink);
                live_mark.startAnimation(animation);
                live_mark.setVisibility(View.VISIBLE);
                capture_button.setText("");
                //drawText("connection enabled");

                //  enableSettingsButton(false);

                createConnection();
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                createVideoEncoder(sp);
                createAudioEncoder(sp);
                createStreamer();



            } else {

                broadcastEnable = false;
                AppConfig.isLive="0";
                Drawable d = getResources().getDrawable(R.drawable.u);
                capture_button.setBackground(d);
                ImageView live_mark=(ImageView)findViewById(R.id.live_mark);

                Animation animation;
                animation = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.blink);
                live_mark.clearAnimation();
                live_mark.setVisibility(View.INVISIBLE);
                capture_button.setText("");

                // enableSettingsButton(true);

                if (-1 != connectionId_) {
                    mStreamer.releaseConnection(connectionId_);
                }
            }
        }
    });

    final Button settings_button = (Button) findViewById(R.id.settings_button);
    settings_button.setOnClickListener(new View.OnClickListener() {
        public void onClick(View v) {
            // Perform action on click
            Log.d("", "!!!Settings!!!");
            //mSettingsDialog.show();

            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);

            startActivity(intent);
        }
    });

    final Button flash_button = (Button) findViewById(R.id.flash_button);
    flash_button.setOnClickListener(new View.OnClickListener() {
        public void onClick(View v) {
            Log.d("", "!!!Flash!!!");

            if(mStreamer.getCameraApi() == Streamer.CAMERA_API.CAMERA) {
                Camera.Parameters param = mStreamer.getCameraParameters();
                String mode = param.getFlashMode();
                if (mode == null)
                    return; //flash not supported

                if (mode.equals(Camera.Parameters.FLASH_MODE_OFF)) {
                    Drawable d = getResources().getDrawable(R.drawable.flashlighton);
                    flash_button.setBackground(d);

                    param.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                } else {
                    param.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    Drawable d = getResources().getDrawable(R.drawable.flashlightoff);
                    flash_button.setBackground(d);
                }
                mStreamer.setCameraParameters(param);
            }
        }
    });
}
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "MainActivity: onPause(), orientation=" + getResources().getConfiguration().orientation);
/*
        broadcastEnable = false;
        if(mStreamer != null ) {
            releaseConnection();

            mStreamer.stopAudioCapture();
            mStreamer.stopVideoCapture();
        }*/
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "MainActivity: onStop(), orientation=" + getResources().getConfiguration().orientation);

        Log.d(TAG, "MainActivity: onStop(), Activity Stoppped" );

        releaseStreamer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "MainActivity: onDestroy(), orientation=" + getResources().getConfiguration().orientation);

        releaseStreamer();
    }

    boolean broadcastEnable = false;
    int connectionId_ = -1;

    long videoPacketsLost_ = 0;
    long audioPacketsLost_ = 0;

    private void createConnection() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        String conn0_uri = sp.getString("conn0_uri", "");

        String username = sp.getString("username", "DEFAULT");
        String password = sp.getString("password", "DEFAULT");
      //  if(conn0_uri.isEmpty()) {

           // Toast.makeText(MainActivity.this, "Empty URI", Toast.LENGTH_LONG).show();
            //return;
      //  }

      //  String conn0_mode = sp.getString("conn0_mode", "");
        Streamer.MODE mode;
      //  if(conn0_mode.equals("0")) {
            mode = Streamer.MODE.AUDIO_VIDEO;
        //} else if(conn0_mode.equals("1")) {
       //     mode = Streamer.MODE.VIDEO_ONLY;
       // } else if(conn0_mode.equals("2")) {
       //     mode = Streamer.MODE.AUDIO_ONLY;
       // } else {
       //     return;
       // }

       // Toast.makeText(MainActivity.this,"rtsp://" + username + ":" + password + "@stream1.ubroadkast.com:1935/" + username + "/livestream",
            //    Toast.LENGTH_LONG).show();
       // String livestreamname, String livepublisher, String livestatus, String livestreamtype, String livestreamlocation, String livequality
        insert("livestream",username,"ACTIVE","ANDROID","rtsp://"+username+":"+password+"@stream1.ubroadkast.com:1935/"+username+"/livestream","NULL","");

        connectionId_ = mStreamer.createConnection("rtsp://" + username + ":" + password + "@stream1.ubroadkast.com:1935/" + username + "/livestream", mode, this);
        if(connectionId_ != -1) {
            setConnectionStatus(Color.GREEN, "Connecting...");
            videoPacketsLost_ = 0;
            audioPacketsLost_ = 0;
        }
    }

    private void releaseConnection() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);


        String username = sp.getString("username", "DEFAULT");
        String password = sp.getString("password", "DEFAULT");

        insert("livestream",username,"STOPPED","ANDROID","rtsp://"+username+":"+password+"@stream1.ubroadkast.com:1935/"+username+"/livestream","NULL","");

        if(connectionId_ == -1) {
            return;
        }

        mStreamer.releaseConnection(connectionId_);
        connectionId_ = -1;
        setConnectionStatus(Color.GREEN, "");
    }


    private void setConnectionStatus(int color, String text) {
        final TextView connectionStatus = (TextView) findViewById(R.id.connectionStatus);
        if(null != connectionStatus) {
            connectionStatus.setTextColor(color);
            connectionStatus.setText(text);
        }
    }

    private void enableSettingsButton(boolean enable) {
        final Button settings_button = (Button) findViewById(R.id.settings_button);
        settings_button.setEnabled(enable);
    }

    String getProfile(byte[] profileLevelId) {
        String profile = "";
        if(profileLevelId == null || profileLevelId.length != 3) {
            return profile;
        }

        // according http://www.itu.int/rec/T-REC-H.264-201304-S/en
        switch (profileLevelId[0]) {
            case 66:
                profile = "Base";
                break;

            case 77:
                profile = "Main";
                break;

            case 88:
                profile = "Extended";
                break;

            case 100:
                profile = "High";
                break;

            case 110:
                profile = "High10";
                break;

            case 122:
                profile = "High422";
                break;

            case (byte) 244:
                profile = "High444";
                break;

            default:
                profile = "Unknown";
                break;
        }
        return profile;
    }

    String timeToString(long time) {
        long ss = time % 60;
        long mm = (time / 60) % 60;
        long hh = time / 3600;
        return String.format("%02d:%02d:%02d", hh, mm, ss);
    }

    String trafficToString(long bytes) {
        if(bytes < 1024) {
            // B
            return String.format("%4dB", bytes);

        } else if( bytes < 1024 * 1024) {
            // KB
            return String.format("%3.1fKB", (double)bytes / 1024);

        } else if(bytes < 1024 * 1024 * 1024) {
            // MB
            return  String.format("%3.1fMB", (double)bytes / (1024 * 1024));
        } else {
            // GB
            return  String.format("%3.1fGB", (double)bytes / (1024 * 1024 * 1024));
        }
    }


    String bandwidthToString(long bps) {
        if(bps < 1000) {
            // bps
            return String.format("%4dbps", bps);

        } else if( bps < 1000 * 1000) {
            // Kbps
            return String.format("%3.1fKbps", (double)bps / 1000);

        } else if(bps < 1000 * 1000 * 1000) {
            // Mbps
            return String.format("%3.1fMbps", (double)bps / (1000 * 1000));
        } else {
            // Gbps
            return String.format("%3.1fGbps", (double)bps / (1000 * 1000 * 1000));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "MainActivity: onCreate(), orientation=" + getResources().getConfiguration().orientation);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
            return;
        }


        mHandler = new Handler(Looper.getMainLooper());

        uiUpateTask = new TimerTask() {
            private DecimalFormat df = new DecimalFormat("#0.0");

            @Override
            public void run() {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(mStreamer == null) {
                            return;
                        }

                        final TextView fpsView = (TextView) findViewById(R.id.fpsView);
                        if(fpsView != null) {
                            double fps = mStreamer.getFps();
                            //byte[] profileLevelId = mStreamer.getProfileLevelId();
                            if(fps >= 0) {
/*
                                if(profileLevelId != null) {
                                    fpsView.setText(getProfile(profileLevelId) + " " + df.format(fps) + " FPS");
                                } else
*/
                                {
                                    fpsView.setText(df.format(fps) + " FPS");
                                }
                            }
                        }


                        if(connectionState_ == Streamer.CONNECTION_STATE.CONNECTED ||
                           connectionState_ == Streamer.CONNECTION_STATE.SETUP ||
                           connectionState_ == Streamer.CONNECTION_STATE.RECORD) {

                            long curTime = System.currentTimeMillis();
                            long duration = (curTime - connTime_) / 1000;

                            long bytesSent =  mStreamer.getBytesSent(connectionId_);
                            long bps=0;
                            if(curTime- prevTime_ <=0){
                                 bps = 8 * 1000 * (bytesSent - prevBytes_);
                            }else {
                                 bps = 8 * 1000 * (bytesSent - prevBytes_) / (curTime - prevTime_);
                            }
                            prevTime_ = curTime;
                            prevBytes_ = bytesSent;

                            String tm = timeToString((long)duration);

                            String tr = trafficToString(bytesSent);

                            String bw = bandwidthToString((long) bps);

                            int color = Color.GREEN;
                            if(audioPacketsLost_ != mStreamer.getAudioPacketsLost(connectionId_) ||
                               videoPacketsLost_ != mStreamer.getVideoPacketsLost(connectionId_)) {
                                //
                                audioPacketsLost_ = mStreamer.getAudioPacketsLost(connectionId_);
                                videoPacketsLost_ = mStreamer.getVideoPacketsLost(connectionId_);
                                color = Color.YELLOW;
                            }

                            setConnectionStatus(color, tm + " " + tr + " " + bw);
                        }

                    }
                });
            }
        };

        uiUpateTimer = new Timer();
        uiUpateTimer.schedule(uiUpateTask, 1000, 1000);
    }

    void drawCameraInfo(Streamer.CameraInfo cameraInfo) {

        String text = "camera: cameraId=" + cameraInfo.cameraId + "; ";

        if(cameraInfo.recordSizes != null) {
            text += "recordSizes=";
            for (Streamer.Size res : cameraInfo.recordSizes) {
                text += res.width + "x" + res.height + "; ";
            }
        }

        if(cameraInfo.previewSizes != null) {
            text += "previewSizes=";
            for (Streamer.Size res : cameraInfo.previewSizes) {
                text += res.width + "x" + res.height + "; ";
            }
        }

        if(cameraInfo.lensFacingBack) {
            text += " back";
        } else {
            text += " front";
        }
        //drawText(text);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    Streamer.Size getPreviewSurfaceSize(int parentWidth, int parentHeight) {

        if(previewSizes == null || previewSizes.length == 0) {
            return null;
        }

        int minHeightDiff = Integer.MAX_VALUE;

        double screenRatio = (double)parentWidth / (double)parentHeight;

        Streamer.Size previewSurfaceSize = null;
        for(Streamer.Size cursor : previewSizes) {
            if(Math.abs(screenRatio - cursor.getRatio()) > 0.1) {
                continue;
            }

            int heightDiff = Math.abs(cursor.height - parentHeight);
            if(heightDiff < minHeightDiff) {
                minHeightDiff = heightDiff;
                previewSurfaceSize = cursor;
            }
        }

        if(previewSurfaceSize != null) {
            return previewSurfaceSize;
        }


        int minSizeDiff = Integer.MAX_VALUE;

        for(Streamer.Size cursor : previewSizes) {
            int widthDiff = Math.abs(cursor.width - parentWidth);
            int heightDiff = Math.abs(cursor.height - parentHeight);
            int sizeDiff =  widthDiff * widthDiff + heightDiff * heightDiff;
            if(sizeDiff < minSizeDiff) {
                minSizeDiff = sizeDiff;
                previewSurfaceSize = cursor;
            }
        }
        return previewSurfaceSize;
    }

    Streamer.Size getPreviewSize(int parentWidth,  int parentHeight, Streamer.Size previewSurfaceSize) {


        double k = Math.min((double) parentWidth / (double) video_size.width,
                (double) parentHeight / (double) video_size.height
        );


        Streamer.Size previewSize = new Streamer.Size(
                (int)(video_size.width * k),
                (int)(video_size.height * k)
        );

        return previewSize;
    }

    void setPreviewSize(int width, int height) {
        SurfaceView view = (SurfaceView)findViewById(R.id.surfaceView);
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.width = width;
        layoutParams.height = height;
        view.setLayoutParams(layoutParams);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d(TAG, "MainActivity: surfaceCreated()");

        if(null == mStreamer) {
            return;
        }

        RelativeLayout frameLayout = (RelativeLayout)findViewById(R.id.camera_preview);
        int parentWidth = frameLayout.getWidth();
        int parentHeight = frameLayout.getHeight();

        Streamer.Size previewSurfaceSize = getPreviewSurfaceSize(parentWidth, parentHeight);
        Streamer.Size previewSize = getPreviewSize(parentWidth, parentHeight, previewSurfaceSize);

        Log.d(TAG, "video " + video_size.width + "x" + video_size.height + " " + video_size.getRatio());
        Log.d(TAG, "surface " + previewSurfaceSize.width + "x" + previewSurfaceSize.height + " " + previewSurfaceSize.getRatio());
        Log.d(TAG, "preview " + previewSize.width + "x" + previewSize.height + " " + previewSize.getRatio());

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        setPreviewSize(previewSize.width, previewSize.height);
        holder.setFixedSize(previewSurfaceSize.width, previewSurfaceSize.height);

        mStreamer.startVideoCapture(this, cameraId, holder, videoEncoder, this);
        mStreamer.startAudioCapture(MediaRecorder.AudioSource.CAMCORDER, audioEncoder, this);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d(TAG, "MainActivity: surfaceChanged() " + width + "x" + height);
        Log.d(TAG, "MainActivity: Resuming connection ");
      //  createConnection();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d(TAG, "MainActivity: surfaceDestroyed()");
    }

    @Override
    public Handler getHandler() {
        return mHandler;
    }

    private Toast toast;

    @Override
    public void OnConnectionStateChanged(int connectionId, Streamer.CONNECTION_STATE state, Streamer.STATUS status) {
        Log.d("", "OnConnectionStateChanged, state=" + state);

        if(null == mStreamer ) {
            return;
        }

        if(connectionId_ == connectionId) {
           connectionState_ = state;
        }

        switch (state) {
            case INITIALIZED:
                //drawText("Connecting... #"+ connectionId);
                break;

            case CONNECTED:
                //drawText("Connected #" + connectionId);
                connTime_ = prevTime_ = System.currentTimeMillis();
                prevBytes_ = mStreamer.getBytesSent(connectionId_);
                break;

            case SETUP:
                //drawText("Setup #" + connectionId);
                break;

            case RECORD:
                //DecimalFormat df = new DecimalFormat("#0.0");
               // drawText("Record #" + connectionId + " fps=" + df.format(mStreamer.getFps()));
                break;

            case DISCONNECTED:
                //drawText("Disconnected #" + connectionId);
                if(connectionId != connectionId_) {
                    Log.e("", "unregistered connection");
                    break;
                }

                int delay = 3000;
                if(broadcastEnable) {
                    if (status == Streamer.STATUS.CONN_FAIL) {
                        setToast("Network failure, could not connect to server.");
                        delay = 3000;
                    } else if (status == Streamer.STATUS.AUTH_FAIL) {
                        setToast("Authentication failure, please check stream credentials.");
                        delay = 3000;
                    } else {
                      //  setToast("Unknown connection failure.");
                        delay = 0;
                    }
                }

                releaseConnection();

                if(broadcastEnable) {
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (broadcastEnable) {
                                createConnection();
                            }
                        }
                    }, delay);
                }
                break;
        }
    }

    @Override
    public void OnVideoCaptureStateChanged(Streamer.CAPTURE_STATE state) {
        Log.d("", "OnVideoCaptureStateChanged, state=" + state);
        if(null == mStreamer ) {
            return;
        }

        switch (state) {
            case STARTED:
                Log.d(TAG, "video capture started");
                break;

            case STOPPED:
                Log.d(TAG, "video capture stopped");
                break;

            case ENCODER_FAIL:
                Log.d(TAG, "video encoder failed");
                mStreamer.stopVideoCapture();
                setToast("Video encoder failed");
                break;

            case FAILED:
            default:
                Log.d(TAG, "video capture failed");
                mStreamer.stopVideoCapture();
                setToast("Video capture failed");
                break;
        }
    }

    private void setToast(String text) {
        if(null != toast) {
            toast.cancel();
        }
        toast = Toast.makeText(this, text, LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void OnAudioCaptureStateChanged(Streamer.CAPTURE_STATE state) {
        Log.d("", "OnAudioCaptureStateChanged, state=" + state);
        if(null == mStreamer ) {
            return;
        }

        switch (state) {
            case STARTED:
                Log.d(TAG, "audio capture started");
                break;

            case STOPPED:
                Log.d(TAG, "audio capture stopped");
                break;

            case ENCODER_FAIL:
                Log.d(TAG, "audio encoder failed");
                mStreamer.stopAudioCapture();
                setToast("Audio encoder failed");
                break;

            case FAILED:
            default:
                Log.d(TAG, "audio capture failed");
                mStreamer.stopAudioCapture();
                setToast("Audio capture failed");
                break;
        }
    }



    public class getShareCodeTask extends AsyncTask<String, Void, String> {
        ProgressDialog pd=  new ProgressDialog(MainActivity.this);

        @Override
        protected void onPreExecute() {
            pd.setMessage("Generate Private Code, please wait.");
           // pd.show();
        }
        @Override
        protected String doInBackground(String... params) {




            String datasent = generateCode();


            return datasent;
        }


        protected void onPostExecute(String result) {

            if (pd.isShowing()) {
                pd.dismiss();
            }
            try {
                JSONObject json_data =  new JSONObject(result);

                ScratchCode=(json_data.getString("sharecode"));
               // Toast.makeText(MainActivity.this, "Result: " +ScratchCode, Toast.LENGTH_SHORT).show();


            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }




    String generateCode() {
        StringBuffer buffer = new StringBuffer();
        try {

            HttpURLConnection conn = (HttpURLConnection) (new URL("http://ubroadkast.com/appServices/generate_sharecode.php")).openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.connect();

            DataOutputStream out = new DataOutputStream(conn.getOutputStream());

            out.write(("").getBytes());

            InputStream is = conn.getInputStream();

            byte[] b = new byte[1024];

            while (is.read(b) != -1)
                buffer.append(new String(b));
            conn.disconnect();
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return buffer.toString();
    }





    private class UpdateBroadkastDB extends AsyncTask<String, Void, String> {
        ProgressDialog pd=  new ProgressDialog(MainActivity.this);

        @Override
        protected void onPreExecute() {
            pd.setMessage("Working, please wait.");
            pd.show();
        }
        @Override
        protected String doInBackground(String... params) {


            String livestreamname=params[0];
            String livepublisher=params[1];
            String livestatus=params[2];
            String livestreamtype=params[3];
            String livestreamlocation=params[4];
            String livequality=params[5];
            String password=params[6];
            //Toast.makeText(MainActivity.this, "ok " , Toast.LENGTH_SHORT).show();

            String datasent = sendHttpRequest(livestreamname, livepublisher,livestatus,livestreamtype,livestreamlocation,livequality,password);

         //   System.out.println("data [" + datasent + "]");
            // System.out.println("URL ["+url+"] - NAME ["+name+"] -PASSWORD ["+password+"]");
            return datasent;
        }


        protected void onPostExecute(String result) {

            if (pd.isShowing()) {
                pd.dismiss();
            }
        }
    }

    String sendHttpRequest(String livestreamname, String livepublisher, String livestatus, String livestreamtype, String livestreamlocation, String livequality,String password) {
        StringBuffer buffer = new StringBuffer();
        try {

            HttpURLConnection conn = (HttpURLConnection) (new URL("http://ubroadkast.com/appServices/broadcastStreamInfo.php")).openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.connect();

            DataOutputStream out = new DataOutputStream(conn.getOutputStream());

            out.write(("streamname=" + livestreamname + "&publisher="+livepublisher+"&status="+livestatus+"&streamtype="+livestreamtype+"&streamlocation="+livestreamlocation+"&quality="+livequality+"&sharecode="+password).getBytes());

            InputStream is = conn.getInputStream();

            byte[] b = new byte[1024];

            while (is.read(b) != -1)
                buffer.append(new String(b));
            conn.disconnect();
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return buffer.toString();
    }
    public void insert(String livestreamname, String livepublisher, String livestatus, String livestreamtype, String livestreamlocation, String livequality,String password)
    {

        UpdateBroadkastDB t = new UpdateBroadkastDB();
        String[] params = new String[]{livestreamname, livepublisher,livestatus,livestreamtype,livestreamlocation,livequality,password};
        t.execute(params);


    }

    private void initShareIntent(String type) {
        boolean found = false;
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("image/jpeg");

        // gets the list of intents that can be loaded.
        List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(share, 0);
        if (!resInfo.isEmpty()){
            for (ResolveInfo info : resInfo) {
                if (info.activityInfo.packageName.toLowerCase().contains(type) ||
                        info.activityInfo.name.toLowerCase().contains(type) ) {
                    share.putExtra(Intent.EXTRA_SUBJECT,  "subject");
                    share.putExtra(Intent.EXTRA_TEXT,     "your text");
                   // share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(myPath)) ); // Optional, just if you wanna share an image.
                    share.setPackage(info.activityInfo.packageName);
                    found = true;
                    break;
                }
            }
            if (!found)
                return;

            startActivity(Intent.createChooser(share, "Select"));
        }
    }

}


