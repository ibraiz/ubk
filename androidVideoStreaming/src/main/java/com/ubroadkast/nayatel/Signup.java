package com.ubroadkast.nayatel;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class Signup extends Activity {
    String id;
    String name;
    String status;
    InputStream is=null;
    String result=null;
    String line=null;
    String responce;
    String scratch;
    private Button loginButton;
    private Button newUserBtn;
    public String getSimSerialNumber;
    private Button newRegisterBtn;
    private Button fblogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_signup);


        Button terms_button=(Button)findViewById(R.id.terms_button);
        terms_button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                termsIntent();

            }
        });

        newRegisterBtn=(Button)findViewById(R.id.ratingSubmit);




        newRegisterBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                TextView phone = (EditText) findViewById(R.id.fbCellNo);
                TextView email = (EditText) findViewById(R.id.fbEmailAddress);
                TextView firstname = (EditText) findViewById(R.id.newUserFirstName);
                TextView lastname = (EditText) findViewById(R.id.newUserLastname);
                final String userPhone = phone.getText().toString();
                final String userEmail = email.getText().toString();
                final String userFirstname = firstname.getText().toString();
               // final String userLastname = lastname.getText().toString();

                if (userPhone.equals("") || userEmail.equals("") || userFirstname.equals("")) {

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.custom_toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("Dear user, Please provide complete information");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();
                    return;
                }


                if (userPhone.equals("") || userEmail.equals("")) {

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.custom_toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("Dear user, Please provide complete information");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();


                    return;
                }
                if (userPhone.length() != 11) {
                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.custom_toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("Valid username is required");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    return;
                }
                if (userPhone.contains("+")) {






                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.custom_toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("Please add valid cell number i:e 0333*******");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    return;
                }


                AppConfig.APP_USER = userPhone;
                AppConfig.APP_EMAIL = userEmail;


                TelephonyManager tel = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

                      /*  TelephonyManager telemamanger = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                 getSimSerialNumber = telemamanger.getSimSerialNumber();
                String getSimNumber = telemamanger.getLine1Number();

*/

                getSimSerialNumber = tel.getDeviceId().toString();

                Register(userPhone, userEmail, userFirstname, "", "NAYATEL", getSimSerialNumber);
//loginIntent();
                //  dialog.dismiss();
            }
        });






    }
    public void loginIntent(){

        Intent intent;
        intent = new Intent(this, login.class);
finish();
        startActivity(intent);


    }
    public void termsIntent()
    {
        Intent intent;
        intent = new Intent(this, Terms.class);

        startActivity(intent);
    }
    public void Register(String phone,String email,String firstname,String lastname,String loginMethod,String imei)
    {
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();


        nameValuePairs.add(new BasicNameValuePair("phone",phone));
        nameValuePairs.add(new BasicNameValuePair("email",email));
        nameValuePairs.add(new BasicNameValuePair("firstname",firstname));
        nameValuePairs.add(new BasicNameValuePair("lastname",lastname));
        nameValuePairs.add(new BasicNameValuePair("loginmethod",loginMethod));

        nameValuePairs.add(new BasicNameValuePair("imei",imei));


        try
        {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://ubroadkast.com/appServices/broadcastReg.php");

            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 1", e.toString());
           // Toast.makeText(getApplicationContext(), e.toString(),
                  //  Toast.LENGTH_LONG).show();
        }

        try
        {
            BufferedReader reader = new BufferedReader
                    (new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 2", e.toString());
        }

        try
        {
            JSONObject json_data = new JSONObject(result);
            responce=(json_data.getString("responce"));
            scratch=(json_data.getString("scratch"));



            Intent intent;
            if(responce.equals("1")){



                AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                builder.setTitle("NOTICE");
                builder.setMessage("Dear User!  You Will Receive Your Credentials Shortly Through SMS & Email");
                builder.setPositiveButton("Ok", null);
                builder.setNegativeButton("Cancel", null);
                builder.show();






             /*   builder.setTitle("NOTICE");
                builder.setMessage("PHONE: "+phone+" SCRATCH:"+scratch);

                builder.show(); */


                Create_User_Wowza_App(phone,scratch,"UBROADKAST_FREE");
                AppConfig.APP_SCRATCH=scratch;
                new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

                    @Override
                    public void run() {

                        finish();
                    }
                }, 2000);

            }
            else if(responce.equals("2")){


                AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                builder.setTitle("NOTICE");
                builder.setMessage("Dear User!  This Account Has Already Been Registered");
                builder.setPositiveButton("Ok", null);
                builder.setNegativeButton("Cancel", null);
                builder.show();

            }
            else if(responce.equals("3")){


                AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                builder.setTitle("NOTICE");
                builder.setMessage("Dear User!  Something Went Wrong, Please Check Your Credentials Again Or Contact Nayatel Support");
                builder.setPositiveButton("Ok", null);
                builder.setNegativeButton("Cancel", null);
                builder.show();




            }
            else{

                AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                builder.setTitle("NOTICE");
                builder.setMessage("Dear User!  Something Went Wrong, Please Check Your Credentials Again Or Contact Nayatel Support ");
                builder.setPositiveButton("Ok", null);
                builder.setNegativeButton("Cancel", null);
                builder.show();




            }





        }
        catch(Exception e)
        {
            Log.e("Fail 3", e.toString());
        }
    }


    public void Create_User_Wowza_App(String phone,String password, String version)
    {
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();


        nameValuePairs.add(new BasicNameValuePair("userid",phone));
        nameValuePairs.add(new BasicNameValuePair("password",password));
        nameValuePairs.add(new BasicNameValuePair("version",version));


        try
        {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://ubroadkast.com/appServices/Wowza-API/addFreeUbroadkastPlanApi.php");
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 1", e.toString());
          //  Toast.makeText(getApplicationContext(), "Invalid IP Address",
                //    Toast.LENGTH_LONG).show();
        }

        try
        {
            BufferedReader reader = new BufferedReader
                    (new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 2", e.toString());
        }

        try
        {
            JSONObject json_data = new JSONObject(result);
            responce=(json_data.getString("responce"));




            Intent intent;
            if(responce.equals("Assigned Successfully")){

                //  Toast.makeText(getApplicationContext(), "Success:"+responce,
                //        Toast.LENGTH_LONG).show();


            }
            else if (responce.equals("Already Exist")){


                // Toast.makeText(getApplicationContext(), "Success:"+responce,
                //  Toast.LENGTH_LONG).show();
            }
            else {
                // Toast.makeText(getApplicationContext(), "ERROR:"+responce,
                //     Toast.LENGTH_LONG).show();
            }







        }
        catch(Exception e)
        {
            Log.e("Fail 3", e.toString());
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_signup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
