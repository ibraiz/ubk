package com.ubroadkast.nayatel;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

public class SplashScreen extends Activity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 2000;
    String terms_flag;
    String Username;
    String Password;
    Intent i;
    final String PREFS_NAME = "MyPrefsFile";
    SharedPreferences settings ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash_screen);

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);


        settings =getSharedPreferences(PREFS_NAME, 0);
        if (settings.getBoolean("my_first_time", true)) {
            //the app is being launched for first time, do something
            Log.d("Comments", "First time");

            // first time task

            /*
            i = new Intent(getApplicationContext(), Terms.class);
            startActivity(i);
            finish();
            */

            // record the fact that the app has been started at least once
            settings.edit().putBoolean("my_first_time", false).commit();

        }
        else{
            Log.e("Comments", "First time++");
        }


        //final Boolean my_first_time = settings.getBoolean("my_first_time",true);

        terms_flag = prefs.getString("terms", "");
        Username = prefs.getString("username","");
        Password = prefs.getString("password","");






       // ImageView image1 = (ImageView)findViewById(R.id.imageView5);
     //   Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade);
      //  image1.startAnimation(animation);
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity

                /*
                terms_flag = prefs.getString("terms", "");
                Username = prefs.getString("username","");
                Password = prefs.getString("password","");
                */

                if( !Username.equals("") && !Password.equals("") && terms_flag.equals("0"))
                {
                    i = new Intent(SplashScreen.this, MainActivity.class); //in case of remember me is checked and both username & password are saved
                    startActivity(i);

                    finish();
                }
                else if( Password.equals("") && terms_flag.equals("0") ){
                    i = new Intent(SplashScreen.this, login.class); //in case remember me is not checked thus password not saved
                    startActivity(i);

                    finish();
                }
                else if (Username.equals("") && Password.equals("") && !terms_flag.equals("0") ){
                    i = new Intent(SplashScreen.this, Terms.class);//in case username & password both are null
                    startActivity(i);
                    finish();

                }

                else {
                    i = new Intent(SplashScreen.this, login.class); //in Unknown case
                    startActivity(i);
                    finish();

                    Log.e("Comments", "unknown");

                }
            }
        }, SPLASH_TIME_OUT);
    }


}