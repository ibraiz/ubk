package com.ubroadkast.nayatel;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;


public class Settings extends FragmentActivity implements ActionBar.TabListener {
    ActionBar actionbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);


        actionbar= getActionBar();
        actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionbar.addTab(actionbar.newTab().setText("Settings").setTabListener(this).setIcon(R.drawable.settings32));
        actionbar.addTab(actionbar.newTab().setText("Live").setTabListener(this).setIcon(R.drawable.camcorder32));
        actionbar.addTab(actionbar.newTab().setText("Home").setTabListener(this).setIcon(R.drawable.home32));


    }
    private void alert(final String msg) {
        final String error = (msg == null) ? "Unknown error: " : msg;
        AlertDialog.Builder builder = new AlertDialog.Builder(Settings.this);
        builder.setMessage(error).setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft){
        Intent intent;
        switch (tab.getPosition()) {

            case 1:
                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;


            case 2:

                intent = new Intent(this, Home.class);
                startActivity(intent);
                break;
            case 3:


                break;


        }
    }


    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft){

        Intent intent;
        switch (tab.getPosition()) {

            case 1:
                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;


            case 2:

                intent = new Intent(this, Home.class);
                startActivity(intent);
                break;
            case 3:


                break;


        }


    }
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft){

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
