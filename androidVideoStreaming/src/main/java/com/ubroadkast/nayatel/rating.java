package com.ubroadkast.nayatel;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class rating extends Activity {
    String id;
    String name;
    String status;
    InputStream is=null;
    String result=null;
    String line=null;
    String responce;
    private Button loginButton;
    private Button newUserBtn;

    private Button newRegisterBtn;
    private Button fblogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_rating);


        Button ratingSubmit=(Button)findViewById(R.id.ratingSubmit);



        ratingSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                RatingBar rating=(RatingBar)findViewById(R.id.ratingBar);
                EditText comments=(EditText)findViewById(R.id.rattingComments);
                rating.setStepSize(1);

                rate(rating.getRating(),comments.getText().toString());


            }
        });


        }


    public void rate(Float rating,String comments)
    {
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();


        nameValuePairs.add(new BasicNameValuePair("rating",String.valueOf(rating)));
        nameValuePairs.add(new BasicNameValuePair("comments",comments));




        try
        {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://ubroadkast.com/appServices/feedback.php");
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 1", e.toString());
            Toast.makeText(getApplicationContext(), "Invalid IP Address",
                    Toast.LENGTH_LONG).show();
        }

        try
        {
            BufferedReader reader = new BufferedReader
                    (new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 2", e.toString());
        }

        try
        {
            JSONObject json_data = new JSONObject(result);
            responce=(json_data.getString("responce"));

            Intent intent;
            if(responce.equals("1")){
                AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                builder.setTitle("Thanks");
                builder.setMessage("Feedback Provided, \n Nayatel Support. +92 (51) 111 11 44 44   OR  99 from Nayatel landline");
                builder.setPositiveButton("Ok", null);
                builder.setNegativeButton("Cancel", null);
                builder.show();
             //   finish();

            }
            else if(responce.equals("2")){

            }
            else if(responce.equals("3")){

            }
            else{

            }





        }
        catch(Exception e)
        {
            Log.e("Fail 3", e.toString());
        }
    }



                    @Override
                    public boolean onCreateOptionsMenu(Menu menu) {
                        // Inflate the menu; this adds items to the action bar if it is present.
                        getMenuInflater().inflate(R.menu.menu_rating, menu);
                        return true;
                    }

                    @Override
                    public boolean onOptionsItemSelected(MenuItem item) {
                        // Handle action bar item clicks here. The action bar will
                        // automatically handle clicks on the Home/Up button, so long
                        // as you specify a parent activity in AndroidManifest.xml.
                        int id = item.getItemId();

                        //noinspection SimplifiableIfStatement
                        if (id == R.id.action_settings) {
                            return true;
                        }

                        return super.onOptionsItemSelected(item);
                    }
                }
